<?php

/**
 * Plugin Name: Base Palupa.
 * Description: Controle base do tema Palupa.
 * Version: 0.1
 * Author: Agência Palupa
 * Author URI: http://www.palupa.com.br
 * Licence: GPL2
 */


function basePalupa () {

		// TIPOS DE CONTEÚDO
		conteudosPalupa();

		// TAXONOMIA

		 taxonomiaPalupa();


		// META BOXES
		metaboxesPalupa();

		// SHORTCODES
		// shortcodesPalupa();

	    // ATALHOS VISUAL COMPOSER
	    //visualcomposerPalupa();

	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosPalupa (){

		// TIPOS DE CONTEÚDO
		tipoVideo();

		tipoEquipe();

		tipoCliente();

		tipoParceiro();

		tipoServico();

		tipoDepoimento();

		tipoProjeto();

		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'video':
					$titulo = 'Título do vídeo';
				break;

				case 'equipe':
					$titulo = 'Nome da pessoa';
				break;

				case 'cliente':
					$titulo = 'Nome do cliente';
				break;

				case 'servico':
					$titulo = 'Nome do serviço';
				break;

				case 'depoimento':
					$titulo = 'Nome do autor';
				break;

				case 'projeto':
					$titulo = 'Nome do cliente / Nome do serviço';
				break;

				default:
				break;
			}

		    return $titulo;

		}

	}


		// CUSTOM POST TYPE VÍDEO
		function tipoVideo() {

			$rotulosVideo = array(
									'name'               => 'Vídeos',
									'singular_name'      => 'Vídeo',
									'menu_name'          => 'Vídeos',
									'name_admin_bar'     => 'Vídeos',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar nova vídeo',
									'new_item'           => 'Novo vídeo',
									'edit_item'          => 'Editar vídeo',
									'view_item'          => 'Ver vídeo',
									'all_items'          => 'Todos os vídeos',
									'search_items'       => 'Buscar vídeo',
									'parent_item_colon'  => 'Dos vídeos',
									'not_found'          => 'Nenhum vídeo cadastrado.',
									'not_found_in_trash' => 'Nenhum vídeo na lixeira.'
								);

			$argsVideo 	= array(
									'labels'             => $rotulosVideo,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-format-video',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'video' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('video', $argsVideo);

		}


		// CUSTOM POST TYPE SERVIÇO
		function tipoServico() {

			$rotulosServico = array(
									'name'               => 'Serviços',
									'singular_name'      => 'Serviço',
									'menu_name'          => 'Serviços',
									'name_admin_bar'     => 'Serviços',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo serviço',
									'new_item'           => 'Novo serviço',
									'edit_item'          => 'Editar serviço',
									'view_item'          => 'Ver serviço',
									'all_items'          => 'Todos os serviços',
									'search_items'       => 'Buscar serviços',
									'parent_item_colon'  => 'Dos serviços',
									'not_found'          => 'Nenhum serviço cadastrado.',
									'not_found_in_trash' => 'Nenhum serviço na lixeira.'
								);

			$argsServico 	= array(
									'labels'             => $rotulosServico,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-admin-tools',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'servicos' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('servico', $argsServico);

		}

		// CUSTOM POST TYPE DEPOIMENTO
		function tipoDepoimento() {

			$rotulosDepoimento = array(
									'name'               => 'Depoimentos',
									'singular_name'      => 'Depoimento',
									'menu_name'          => 'Depoimentos',
									'name_admin_bar'     => 'Depoimentos',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo depoimento',
									'new_item'           => 'Novo depoimento',
									'edit_item'          => 'Editar depoimento',
									'view_item'          => 'Ver depoimento',
									'all_items'          => 'Todos os depoimentos',
									'search_items'       => 'Buscar depoimentos',
									'parent_item_colon'  => 'Dos depoimentos',
									'not_found'          => 'Nenhum depoimento cadastrado.',
									'not_found_in_trash' => 'Nenhum depoimento na lixeira.'
								);

			$argsDepoimento 	= array(
									'labels'             => $rotulosDepoimento,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-heart',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'depoimento' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('depoimento', $argsDepoimento);

		}


		// CUSTOM POST TYPE COLABORADOR
		function tipoEquipe() {

			$rotulosEquipe = array(
									'name'               => 'Equipe',
									'singular_name'      => 'Equipe',
									'menu_name'          => 'Equipe',
									'name_admin_bar'     => 'Equipe',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo membro',
									'new_item'           => 'Novo membro',
									'edit_item'          => 'Editar membro',
									'view_item'          => 'Ver membro',
									'all_items'          => 'Todos os membros',
									'search_items'       => 'Buscar membros',
									'parent_item_colon'  => 'Dos membros',
									'not_found'          => 'Nenhum membro cadastrado.',
									'not_found_in_trash' => 'Nenhum membro na lixeira.'
								);

			$argsEquipe 	= array(
									'labels'             => $rotulosEquipe,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-nametag',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'equipe' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('equipe', $argsEquipe);

		}


		// CUSTOM POST TYPE CLIENTE
		function tipoCliente() {

			$rotulosCliente = array(
									'name'               => 'Clientes',
									'singular_name'      => 'cliente',
									'menu_name'          => 'Clientes',
									'name_admin_bar'     => 'Clientes',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo cliente',
									'new_item'           => 'Novo cliente',
									'edit_item'          => 'Editar cliente',
									'view_item'          => 'Ver cliente',
									'all_items'          => 'Todos os clientes',
									'search_items'       => 'Buscar clientes',
									'parent_item_colon'  => 'Dos clientes',
									'not_found'          => 'Nenhum cliente cadastrado.',
									'not_found_in_trash' => 'Nenhum cliente na lixeira.'
								);

			$argsCliente 	= array(
									'labels'             => $rotulosCliente,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-businessman',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'clientes' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('cliente', $argsCliente);

		}

			// CUSTOM POST TYPE PARCEIRO
		function tipoParceiro() {

			$rotulosParceiro = array(
									'name'               => 'Parceiros',
									'singular_name'      => 'parceiro',
									'menu_name'          => 'Parceiros',
									'name_admin_bar'     => 'Parceiros',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo parceiro',
									'new_item'           => 'Novo parceiro',
									'edit_item'          => 'Editar parceiro',
									'view_item'          => 'Ver parceiro',
									'all_items'          => 'Todos os parceiros',
									'search_items'       => 'Buscar parceiros',
									'parent_item_colon'  => 'Dos parceiros',
									'not_found'          => 'Nenhum parceiro cadastrado.',
									'not_found_in_trash' => 'Nenhum parceiro na lixeira.'
								);

			$argsParceiro 	= array(
									'labels'             => $rotulosParceiro,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-universal-access',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'parceiros' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('parceiros', $argsParceiro);

		}

		// CUSTOM POST TYPE PROJETO
		function tipoProjeto() {

			$rotulosProjeto = array(
									'name'               => 'Projetos',
									'singular_name'      => 'projeto',
									'menu_name'          => 'Projetos',
									'name_admin_bar'     => 'Projetos',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo projeto',
									'new_item'           => 'Novo projeto',
									'edit_item'          => 'Editar projeto',
									'view_item'          => 'Ver projeto',
									'all_items'          => 'Todos os projetos',
									'search_items'       => 'Buscar projetos',
									'parent_item_colon'  => 'Dos projetos',
									'not_found'          => 'Nenhum projeto cadastrado.',
									'not_found_in_trash' => 'Nenhum projeto na lixeira.'
								);

			$argsProjeto 	= array(
									'labels'             => $rotulosProjeto,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-portfolio',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'projeto' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('projeto', $argsProjeto);

		}


	/****************************************************
	* TAXONOMIA
	*****************************************************/

	function taxonomiaPalupa () {

		taxonomiaCategoriaServico();

//		taxonomiaCategoriaCliente();

	}

		//TAXONOMIA DE SERVIÇO
		function taxonomiaCategoriaServico() {

			$rotulosCategoriaServico = array(
												'name'              => 'Categorias de serviço',
												'singular_name'     => 'Categoria de serviço',
												'search_items'      => 'Buscar categorias de serviço',
												'all_items'         => 'Todas as categorias de serviço',
												'parent_item'       => 'Categoria de serviço pai',
												'parent_item_colon' => 'Categoria de serviço pai:',
												'edit_item'         => 'Editar categoria de serviço',
												'update_item'       => 'Atualizar categoria de serviço',
												'add_new_item'      => 'Nova categoria de serviço',
												'new_item_name'     => 'Nova categoria',
												'menu_name'         => 'Categorias de serviço',
											);

			$argsCategoriaServico 		= array(
												'hierarchical'      => true,
												'labels'            => $rotulosCategoriaServico,
												'show_ui'           => true,
												'show_admin_column' => true,
												'query_var'         => true,
												'rewrite'           => array( 'slug' => 'categoria-servico' ),
											);

			register_taxonomy( 'categoriaServico', array( 'servico' ), $argsCategoriaServico );

		}




	// 	// TAXONOMIA DE CLIENTE
	// 	function taxonomiaCategoriaCliente() {

	// 		$rotulosCategoriaCliente = array(
	// 											'name'              => 'Categorias de Cliente',
	// 											'singular_name'     => 'Categoria de Cliente',
	// 											'search_items'      => 'Buscar categorias de Cliente',
	// 											'all_items'         => 'Todas categorias de Cliente',
	// 											'parent_item'       => 'Categoria de Cliente pai',
	// 											'parent_item_colon' => 'Categoria de Cliente pai:',
	// 											'edit_item'         => 'Editar categoria de Cliente',
	// 											'update_item'       => 'Atualizar categoria de Cliente',
	// 											'add_new_item'      => 'Nova categoria de Cliente',
	// 											'new_item_name'     => 'Nova categoria',
	// 											'menu_name'         => 'Categorias de Cliente',
	// 										);

	// 		$argsCategoriaCliente 		= array(
	// 											'hierarchical'      => true,
	// 											'labels'            => $rotulosCategoriaCliente,
	// 											'show_ui'           => true,
	// 											'show_admin_column' => true,
	// 											'query_var'         => true,
	// 											'rewrite'           => array( 'slug' => 'categoria-cliente' ),
	// 										);

	// 		register_taxonomy( 'categoriaCliente', array( 'cliente' ), $argsCategoriaCliente );

	//}


    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesPalupa(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'Palupa_';


			/**************************
			*	METABOX DE SERVIÇOS
			**************************/
			$metaboxes[] = array(

				'id'			=> 'detalhesServicoMetabox',
				'title'			=> 'Detalhes do Serviço',
				'pages' 		=> array( 'servico' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Subtítulo : ',
						'id'    => "{$prefix}servico_subtitulo",
						'desc'  => '',
						'type'  => 'text'
					),

					array(
                        'name'  		   => 'Ícone do serviço : ',
                        'id'    		   => "{$prefix}servico_icone",
                        'desc'  		   => '',
                        'type'  		   => 'image_advanced',
                        'max_file_uploads' => 1
                    ),

					array(
						'name'  		   => 'Imagem do item : ',
						'id'    		   => "{$prefix}servico_imagemItem",
						'desc'  		   => 'Imagem do serviço no archive',
						'type'  		   => 'image_advanced',
						'max_file_uploads' => 1
					),
					array(
						'name'  	 => 'Clientes que utilizam o serviço: ',
						'id'    	 => "{$prefix}servico_utilizado_cliente",
						'desc'  	 => '',
						'type'  	 => 'post',
						'post_type'  => 'cliente',
						'field_type' => 'select',
						'multiple' => true
					),


					array(
						'name'  => 'Lista de itens do  serviço: ',
						'id'    => "{$prefix}servico_lista",
						'desc'  => '',
						'type'  => 'text',
						'clone' => true
					),


					array(
						'name'  => 'Detalhes do serviço superior da página',
						'id'    => "{$prefix}servico_chamada1_texto",
						'desc'  => 'Texto',
						'type'  => 'textarea',
						'placeholder'  => 'Detalhes do serviço da parte superior'
					),
					array(
						'name'  => '.',
						'id'    => "{$prefix}servico_chamada1_porcentagem",
						'desc'  => 'Porcentagem',

						'type'  => 'text',
						'placeholder'  => '200;Mais'

					),

					array(
						'name'  => 'Detalhes do serviço parte inferior dá página',
						'id'    => "{$prefix}servico_chamada2_texto",
						'desc'  => 'Texto',
						'type'  => 'textarea',
						'placeholder'  => 'Detalhes do serviço da parte inferior'
					),
					array(
						'name'  => '. ',
						'id'    => "{$prefix}servico_chamada2_porcentagem",
						'desc'  => 'Porcentagem',

						'type'  => 'text',
						'placeholder'  => '200;Mais'

					),
					array(
						'name'  => 'Serviços área logo quadro branco inferior',
						'id'    => "{$prefix}servico_area_logo",
						'type'  => 'textarea',
						'placeholder'  => 'Descrição quadro brnaco logo',

					),
					array(
						'name'  => 'Serviços área logo quadro branco superior',
						'id'    => "{$prefix}servico_area_logo_superior",
						'type'  => 'textarea',
						'placeholder'  => 'Descrição quadro brnaco logo',

					),
					array(
						'name'  => 'Serviços área logo quadro rosa',
						'id'    => "{$prefix}servico_quadro_rosa_titulo",
						'type'  => 'text',
						'placeholder'  => 'Título',
						'desc'  => 'Título',


					),
					array(
						'name'  => '.',
						'id'    => "{$prefix}servico_quadro_rosa_texto",
						'type'  => 'textarea',
						'placeholder'  => 'Descrição',
						'desc'  => 'Descrão ',
					),

					array(
						'name'  => 'Frase  do rodapé da página',
						'id'    => "{$prefix}servico_rodape",
						'type'  => 'textarea',
						'placeholder'  => 'Descrição',
						'desc'  => 'Lado esquerdo',
					),
					array(
						'name'  => '. ',
						'id'    => "{$prefix}servico_rodape_porcentagem_esquerdo",
						'desc'  => 'Porcentagem',
						'type'  => 'number',
						'placeholder'  => '50'
					),
					array(
						'name'  => 'Segunda frase  do rodapé da página',
						'id'    => "{$prefix}servico_rodape2",
						'type'  => 'textarea',
						'placeholder'  => 'Descrição',
						'desc'  => 'Lado direito',
					),
					array(
						'name'  => '. ',
						'id'    => "{$prefix}servico_rodape_porcentagem_direito",
						'desc'  => 'Porcentagem',
						'type'  => 'number',
						'placeholder'  => '200'
					),

					array(
						'name'  		   => 'Logos lado esquerdo: ',
						'id'    		   => "{$prefix}cliente_logo_rodape_esquerdo",
						'desc'  		   => '',
						'type'  		   => 'image_advanced',
						'max_file_uploads' => 4
					),
					array(
						'name'  		   => 'Logos lado direito: ',
						'id'    		   => "{$prefix}cliente_logo_rodape_direito",
						'desc'  		   => '',
						'type'  		   => 'image_advanced',
						'max_file_uploads' => 4
					)
				)
			);


			/**************************
			*	METABOX DE CLIENTES
			**************************/
			$metaboxes[] = array(

				'id'			=> 'detalhesClienteMetabox',
				'title'			=> 'Detalhes do Cliente',
				'pages' 		=> array( 'cliente' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Ocultar cases: ',
						'id'    => "{$prefix}servico_esconder",
						'desc'  => '',
						'type'  => 'checkbox',

					),

					array(
						'name'  => 'Subtítulo no cabeçalho da página: ',
						'id'    => "{$prefix}cliente_subtitulo",
						'desc'  => '',
						'type'  => 'text'
					),

					array(
						'name'  		   => 'Imagem de fundo: ',
						'id'    		   => "{$prefix}cliente_imagem_fundo",
						'desc'  		   => 'Imagem de fundo no cabeçalho e na miniatura do botão "Próximo Cliente"',
						'type'  		   => 'image_advanced',
						'max_file_uploads' => 1
					),
					array(
						'name'  		   => 'Imagem mocape: ',
						'id'    		   => "{$prefix}cliente_logo_imagem_mocape",
						'desc'  		   => '',
						'type'  		   => 'image_advanced',
						'max_file_uploads' => 1
					),

					array(
						'name'  => 'Lista de informações: ',
						'id'    => "{$prefix}cliente_listaInfo",
						'desc'  => 'Informações relevantes sobre os projetos realizados para o cliente',
						'type'  => 'textarea',
						'clone' => true,
						'placeholder' => 'Depoimento | url do icone'
					),

					array(
						'name' 			   => 'Logo: ',
						'id'   			   => "{$prefix}cliente_logo",
						'desc' 			   => '',
						'type' 			   => 'image_advanced',
						'max_file_uploads' => 1
					),

					array(
						'name' 			   => 'Logo branca: ',
						'id'   			   => "{$prefix}cliente_logo_branca",
						'desc' 			   => '',
						'type' 			   => 'image_advanced',
						'max_file_uploads' => 1
					),

					array(
						'name' 			   => 'Cor do cliente: ',
						'id'   			   => "{$prefix}cliente_cor",
						'desc' 			   => '',
						'type' 			   => 'color',
					),

					array(
						'name'  => 'Link do vídeo do cliente: ',
						'id'    => "{$prefix}cliente_video",
						'desc'  => '',
						'type'  => 'text',
					),

					array(
						'name' 			   => 'Depoimento do cliente: ',
						'id'   			   => "{$prefix}cliente_depoimento",
						'desc' 			   => '',
						'type' 			   => 'textarea',
						'placeholder'	   => 'Depoimento do cliente'
					),
					array(
						'name' 			   => 'Nome do cliente: ',
						'id'   			   => "{$prefix}cliente_depoimento_nome",
						'desc' 			   => '',
						'type' 			   => 'text',
						'placeholder'	   => 'Nome do cliente'
					),
					array(
						'name' 			   => 'Foto deo cliente depoimento: ',
						'id'   			   => "{$prefix}cliente_foto_depoimento",
						'desc' 			   => '',
						'type' 			   => 'image',
						'max_file_uploads' => 1
					),

					array(
						'name' 			   => 'Galeria de serviços case: ',
						'id'   			   => "{$prefix}cliente_galeria_case",
						'desc' 			   => 'Imagens | Caso queira incluir url do vídeo do youtube na descrição da imagem.',
						'type' 			   => 'image_advanced',
						'max_file_uploads' => 9
					),

					array(
						'name' 			   => 'Fotos para a área de anterior e próximo: ',
						'id'   			   => "{$prefix}cliente_antiprox",
						'desc' 			   => '',
						'type' 			   => 'image_advanced',
						'max_file_uploads' => 2
					),

				)
			);


			$metaboxes[] = array(

				'id'			=> 'detalhesparceiroMetabox',
				'title'			=> 'Detalhes do parceiro',
				'pages' 		=> array( 'parceiros' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

				
					array(
						'name' 			   => 'Logo branca: ',
						'id'   			   => "{$prefix}parceiros_logo_branca",
						'desc' 			   => '',
						'type' 			   => 'image_advanced',
						'max_file_uploads' => 1
					),


				)
			);



			/**************************
			*	METABOX DE PROJETO
			**************************/
			$metaboxes[] = array(

				'id'			=> 'detalhesProjetoMetabox',
				'title'			=> 'Detalhes do Projeto',
				'pages' 		=> array( 'projeto' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Nome do projeto: ',
						'id'    => "{$prefix}projeto_nome_projeto",
						'desc'  => 'Ex: Aplicativo, Site Responsivo, Adwords, etc...',
						'type'  => 'text'
					),

					array(
						'name'  => 'Ícone do projeto: ',
						'id'    => "{$prefix}projeto_icone",
						'type' 			   => 'image_advanced',
						'max_file_uploads' => 1
					),

					array(
						'name'  	 => 'Cliente: ',
						'id'    	 => "{$prefix}projeto_cliente",
						'desc'  	 => '',
						'type'  	 => 'post',
						'post_type'  => 'cliente',
						'field_type' => 'select'
					),

					array(
						'name'  	 => 'Serviço: ',
						'id'    	 => "{$prefix}projeto_servico",
						'desc'  	 => '',
						'type'  	 => 'post',
						'post_type'  => 'servico',
						'field_type' => 'select'
					),

					array(
						'name' 			   => 'Imagem principal: ',
						'id'   			   => "{$prefix}projeto_imagem",
						'desc' 			   => '',
						'type' 			   => 'image_advanced',
						'max_file_uploads' => 1
					),

					array(
						'name'  		   => 'Imagem secundária: ',
						'id'    		   => "{$prefix}projeto_imagem2",
						'desc'  		   => '',
						'type'  		   => 'image_advanced',
						'max_file_uploads' => 1
					),

					array(
						'name'  => 'Conteúdo Principal: ',
						'id'    => "{$prefix}projeto_conteudo1_titulo",
						'desc'  => 'Título',
						'type'  => 'text'
					),

					array(
						'name'  => '.',
						'id'    => "{$prefix}projeto_conteudo1_texto",
						'desc'  => 'Texto',
						'type'  => 'textarea'
					),

					array(
						'name'  => 'Conteúdo 2: ',
						'id'    => "{$prefix}projeto_conteudo2_titulo",
						'desc'  => 'Título',
						'type'  => 'text'
					),

					array(
						'name'  => '.',
						'id'    => "{$prefix}projeto_conteudo2_texto",
						'desc'  => 'Texto',
						'type'  => 'textarea'
					),

					array(
						'name'  => 'Conteúdo 3: ',
						'id'    => "{$prefix}projeto_conteudo3_titulo",
						'desc'  => 'Título',
						'type'  => 'text'
					),

					array(
						'name'  => '.',
						'id'    => "{$prefix}projeto_conteudo3_texto",
						'desc'  => 'Texto',
						'type'  => 'textarea'
					),

					array(
						'name' 			   => 'Cor da logo 1: ',
						'id'   			   => "{$prefix}projeto_cor_logo1",
						'desc' 			   => '',
						'type' 			   => 'color',
					),

					array(
						'name' 			   => 'Cor da logo 2: ',
						'id'   			   => "{$prefix}projeto_cor_logo2",
						'desc' 			   => '',
						'type' 			   => 'color',
					),

					array(
						'name' 			   => 'Cor da logo 3: ',
						'id'   			   => "{$prefix}projeto_cor_logo3",
						'desc' 			   => '',
						'type' 			   => 'color',
					),

					array(
						'name' 			   => 'Cor da logo 4: ',
						'id'   			   => "{$prefix}projeto_cor_logo4",
						'desc' 			   => '',
						'type' 			   => 'color',
					),
				)
			);


			/****************************
			*	METABOX DE COLABORADOR
			*****************************/
			$metaboxes[] = array(

				'id'			=> 'detalhesEquipeMetabox',
				'title'			=> 'Detalhes do Equipe',
				'pages' 		=> array( 'equipe' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Especialidade : ',
						'id'    => "{$prefix}equipe_especialidade",
						'desc'  => '',
						'type'  => 'text'
					),
				)
			);


			/****************************
			*	METABOX DE DEPOIMENTOS
			*****************************/
			$metaboxes[] = array(

				'id'			=> 'detalhesDepoimentoMetabox',
				'title'			=> 'Detalhes do Depoimento',
				'pages' 		=> array( 'depoimento' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  	 => 'Cliente: ',
						'id'    	 => "{$prefix}depoimento_cliente",
						'desc'  	 => '',
						'type'  	 => 'post',
						'post_type'  => 'cliente',
						'field_type' => 'select'
					),

					array(
						'name'  	 => 'Serviço: ',
						'id'    	 => "{$prefix}depoimento_servico",
						'desc'  	 => '',
						'type'  	 => 'post',
						'post_type'  => 'servico',
						'field_type' => 'select'
					),
				)
			);


			/************************
			*	METABOX DE VÍDEOS
			*************************/
			$metaboxes[] = array(

				'id'			=> 'detalhesVideoMetabox',
				'title'			=> 'Detalhes do Video',
				'pages' 		=> array( 'video' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Link do vídeo : ',
						'id'    => "{$prefix}video_link",
						'desc'  => '',
						'type'  => 'text'
					)
				)
			);

			/************************
			*	METABOX DE POST
			*************************/
			$metaboxes[] = array(

				'id'			=> 'detalhesPost',
				'title'			=> 'Detalhes do post',
				'pages' 		=> array( 'post' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Autor: ',
						'id'    => "{$prefix}video_autor",
						'desc'  => '',
						'type'  => 'text'
					)
				)
			);


			return $metaboxes;
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesPalupa(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerPalupa(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'basePalupa');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	//add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	basePalupa();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );