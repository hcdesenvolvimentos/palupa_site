<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package palupa
 */
global $post;
$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$foto = $foto[0];

//var_dump($post);


get_header(); ?>

	<!-- PÁGINA DE SERVIÇO DETALHADO-->
	<div class="pg pg-servico-detalhado">

		<!-- TÍTULO PÁGINA -->
		<div class="tituloPagina">
			<span>Serviço</span>
		</div>

		<!-- BTN MENU SERVIÇO -->
		<div class="areaBtnMenuCliente">
			<button class="navTrigger navTriggerServicos" id="btnAbrirMenuLateral" ><i class="fa fa-th"></i></button>
			<button class="navTrigger navTriggerServicos" id="btnFecharMenuLateral"><i class="fa fa-times"></i></button>
			<p id="tituloMenuInternas">Serviços</p>
		</div>

		<!-- MENU SERVIÇO -->
		<nav class="menu-lateral menu-lateralServicos">

			<ul class="listaMenuLateral">
				<!-- LOOP DE SERVIÇOS / SIDEBAR -->
				<?php
				$terms = get_the_terms( $post->ID , 'categoriaServico' );
				$taxonomiaSlug = $terms[0]->slug;
				$sidebarServicos = new WP_Query(array(
					'post_type' => 'servico',
					'posts_per_page' 	=> -1,
					'tax_query' 		=> array(
						array(
							'taxonomy' => 'categoriaServico',
							'field'    => 'slug',
							'terms'    => $taxonomiaSlug,
							)
						)
					)
				);
				while ( $sidebarServicos->have_posts() ) : $sidebarServicos->the_post();
				?>
				<li class="menu-item"><a href="<?php echo get_permalink(); ?>" title="<?php echo get_the_title() ?>"><?php echo get_the_title() ?></a></li>
				<?php endwhile; wp_reset_query(); ?>
			</ul>

		</nav>

		<!-- ANIMAÇÃO TOPO -->
		<div class="areaAnimacaoTopo">
			<!-- INFORMAÇÕES PRINCIPAIS TOPO SERVIÇOS  -->

			<section class="info-servico" style="background:url(<?php echo $foto ?> )no-repeat!important;background-position: center!important; background-size: cover!important;">

				<div class="lente">
					<ul>
						<li class="lista-texto">
							<div class="area-texto-servico">
								<h4><?php echo get_the_title() ?></h4>
								<strong><?php echo rwmb_meta('Palupa_servico_subtitulo'); ?></strong>
								<?php echo the_content() ?>

							</div>
						</li>
						<li class="lista-check">
							<div class="descricoes">
								<!-- LOOP ITENS DA LISTA-->
								<?php
									$itensListas = rwmb_meta('Palupa_servico_lista');
									foreach ($itensListas as $itensListas => $item) {

								 ?>

									<p><?php echo $item ?></p>

								<?php } ?>

							</div>
						</li>
					</ul>
				</div>

			</section>
		</div>

		<!-- ANIMAÇÃO RODAPE -->
		<div class="areaAnimacaoRodape">
			<!-- CARROSSEL DEPOIMENTOS -->
			<div class="container">
				<!-- CARROSSEL -->
				<?php

					// LOOP DE DEPOIMENTOS
					$depoimetosClientes = new WP_Query( array( 'post_type' => 'depoimento', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1, 'meta_query' => array( array( 'key' => 'Palupa_depoimento_servico', 'value' => $post->ID, 'compare' => 'LIKE' ) )) );

					if ($depoimetosClientes->have_posts()):
				 ?>
				<div class="carrossel">

					<!-- BOTÕES CARROSSEL -->
					<div class="botoes-destaque">
						<button class="navegacaoDepoimentoFrent hidden-xs"><i class="fa fa-angle-left"></i></button>
						<button class="navegacaoDepoimentoTras hidden-xs"><i class="fa fa-angle-right"></i></button>
					</div>

					<div id="carrossel-depoimentos" class="owl-Carousel">
						<!-- ITEM DO CARROSSEL DEPOIMENTOS -->
						<?php
						$clientesQueUsamServico = rwmb_meta('Palupa_servico_utilizado_cliente');
						$postID = $post->ID;


						while ( $depoimetosClientes->have_posts() ) : $depoimetosClientes->the_post();
						$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$foto = $foto[0];
						$idClienteDepoimento = rwmb_meta('Palupa_depoimento_cliente');
						$idServicoDepoimento = rwmb_meta('Palupa_depoimento_servico');
						$nomeDepoimento = get_the_title();
						$depoimentoTetxto = get_the_content();

							// LOOP DE CLIENTES PARA PEGAR NOME DO CLIENTE
							$clientePostDepoimento = new WP_Query( array( 'post_type' => 'cliente', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );
							while ( $clientePostDepoimento->have_posts() ) : $clientePostDepoimento->the_post();

								if ($idClienteDepoimento == $post->ID) {
									$nomeClienteDepoimento = get_the_title();
								}

							endwhile; wp_reset_query();

						if ($postID == $idServicoDepoimento ) {


						?>
						<div class="item">
							<div class="depoimetos">
								<p><?php echo $depoimentoTetxto; ?></p>
								<div class="foto" style="background: url(<?php echo $foto  ?>);"></div>
								<span><?php echo $nomeDepoimento; ?> <?php echo $nomeClienteDepoimento; ?></span>
							</div>
						</div>
						<?php } endwhile;wp_reset_query(); ?>

					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>

		<!-- ÁREA DE CLIENTES  -->
		<section>

			<!-- ÁREA SOBRE PALUPA -->
			<div class="area-sobre-servico">

				<div class="row">

					<div class="col-md-3 text-right">
						<?php
						$porcentagemInfo = rwmb_meta('Palupa_servico_chamada1_porcentagem');
						$porcentagemInfo1 = explode(";", $porcentagemInfo);

						?>
						<p class="infoTop"><span class="number" data-stop="<?php echo $porcentagemInfo1[0]; ?>"><?php echo $porcentagemInfo1[0]; ?></span> <b><?php echo $porcentagemInfo1[1]; ?></b></p>
						<div class="icon"><img class="img-responsive shake-chunk" src="<?php bloginfo('template_directory'); ?>/img/crosshair.svg" alt=""></div>
					</div>

					<!-- DESCRIÇÕES PÁGINA -->
					<div class="col-md-9">
						<p class="texto-sobre"><?php echo rwmb_meta('Palupa_servico_chamada1_texto'); ?></p>
					</div>

				</div>

			</div>

			<!-- CLIENTES -->
			<ul class="clientes-servico">
				<?php
					$servico_area_logo = rwmb_meta('Palupa_servico_area_logo');
					$servico_area_logoSuperior = rwmb_meta('Palupa_servico_area_logo_superior');
					$servico_quadro_rosa_titulo = rwmb_meta('Palupa_servico_quadro_rosa_titulo');
					$servico_quadro_rosa_texto = rwmb_meta('Palupa_servico_quadro_rosa_texto');
					$clientes = rwmb_meta('Palupa_servico_utilizado_cliente');
					$clientePost = new WP_Query( array( 'post_type' => 'cliente', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );
					$i = 0;

					// LOOP DE POST CLIENTES
					while ( $clientePost->have_posts() ) : $clientePost->the_post();
						$ocultarCase      = rwmb_meta('Palupa_servico_esconder');
						// LOOP PARA PEGAR METABOX DE CADA CLIENTE
						foreach ($clientes as $cliente):

							// VERIFICANDO SE ID DO CLIENTE É O MESMO DO SERVIÇO
							if ($post->ID == $cliente):
								$logoBrancoCliente = rwmb_meta('Palupa_cliente_logo_branca','type=image');
								$logoBrancoCliente = array_values($logoBrancoCliente)[0];
								$logoBrancoCliente = $logoBrancoCliente['full_url'];

				?>

								<?php
									//VERIFICACÇÃO SE FOR 1  MOSTRAR DESCRIÇÃO SERVIÇO
									if ($i == 1):
								?>
									<!-- DESCRIÇÃO SERVIÇOS PALUPA -->
									<li class="cliente-servicos">
										<a href="<?php echo home_url('/contato'); ?>" title="Contato">
											<p><?php echo $servico_area_logo;  ?></p>
										</a>
									</li>


								<!-- VEFIRICAÇÃO SE FOR 6 MOSTRAR OUTRO SERVIÇO -->
								<?php elseif ($i == 2 ): ?>

									<!-- SERVIÇOS PALUPA  QUADRO ROSA-->
									<li class="cliente-info-servicos">

										<div class="icon">
											<img src="<?php bloginfo('template_directory'); ?>/img/responsive.svg" alt="" class="img-responsive">
										</div>

										<!-- CHAMADA -->
										<h4><?php echo $servico_quadro_rosa_titulo ?></h4>

										<!-- DESCRIÇÕES -->
										<p><?php echo $servico_quadro_rosa_texto ?></p>

										<div class="caixa-icon">
											<div class="prog">
												<div class="areaImgNumero">
													<img src="<?php bloginfo('template_directory'); ?>/img/two-branches-with-leaves-symmetrical-symbol.svg">
													<span class="valorPorcentagem" data-stop="65">65</span>
													<span>%</span>
												</div>
												<div class="valorCirculo" id="valorCirculo" data-valorPorcentagem="65">
													<svg viewBox="-10 -10 220 220">
														<g fill="none" stroke-width="8" transform="translate(100,100)">
															<path d="M 0,-100 A 100,100 0 0,1 86.6,-50" stroke="#fb9db4"/>
															<path d="M 86.6,-50 A 100,100 0 0,1 86.6,50" stroke="#fb9db4"/>
															<path d="M 86.6,50 A 100,100 0 0,1 0,100" stroke="#fb9db4"/>
															<path d="M 0,100 A 100,100 0 0,1 -86.6,50" stroke="#fb9db4"/>
															<path d="M -86.6,50 A 100,100 0 0,1 -86.6,-50" stroke="#fb9db4"/>
															<path d="M -86.6,-50 A 100,100 0 0,1 0,-100" stroke="#fb9db4"/>
														</g>
													</svg>
													<svg id="porcentagemValorCirculo" viewBox="-10 -10 220 220">
														<path d="M200,100 C200,44.771525 155.228475,0 100,0 C44.771525,0 0,44.771525 0,100 C0,155.228475 44.771525,200 100,200 C155.228475,200 200,155.228475 200,100 Z"></path>
													</svg>
												</div>
											</div>
										</div>

									</li>

								<?php endif; ?>


								<!-- MOSTRANDO LOGO DO CLIENTE QUE ULTILIZA O SERVIÇO -->
								<?php if ($logoBrancoCliente != null): ?>
									<!-- CLIENTE LOGO -->
									<?php  if ($ocultarCase == 0):?>
									<li class="cliente">
										<a href="<?php echo get_permalink(); ?>">
											<div class="lente">
												<div class="logo-marca">
													<img src="<?php echo $logoBrancoCliente  ?>" class="img-responsive" alt="">
												</div>
												<p>Conheça esse cliente !</p>
											</div>
										</a>
									</li>
								<?php else: ?>
									<li class="cliente">

										<div class="lente">
											<div class="logo-marca">
												<img src="<?php echo $logoBrancoCliente  ?>" class="img-responsive" alt="">
											</div>
											<p>Conheça esse cliente !</p>
										</div>

									</li>
								<?php endif; endif; ?>

						<?php endif; endforeach ?>

					<?php $i++; endwhile;wp_reset_query(); ?>

					<li class="cliente-servicos">
						<a href="<?php echo home_url('/contato'); ?>" title="Contato">
							<p><?php echo $servico_area_logoSuperior?> </p>
						</a>
					</li>

				</ul>

				<!-- ÁREA SOBRE PALUPA -->
				<div class="area-sobre-servico rodape">
					<div class="row">

						<!-- DESCRIÇÕES -->
						<div class="col-md-8">
							<p class="texto-sobre"><?php echo rwmb_meta('Palupa_servico_chamada2_texto'); ?> </p>
						</div>

						<!-- ICONES -->
						<div class="col-md-4 text-center">
							<div class="icon">
								<img class="img-responsive img-rotate" src="<?php bloginfo('template_directory'); ?>/img/crosshair.svg" alt="">
							</div>
							<?php
							$porcentagemInfo = rwmb_meta('Palupa_servico_chamada2_porcentagem');
							$porcentagemInfo = explode(";", $porcentagemInfo);

							?>
							<p class="info"><span  class="numberB" data-stop="<?php echo $porcentagemInfo[0]; ?>"><?php echo $porcentagemInfo[0]; ?></span> <b><?php echo $porcentagemInfo[1]; ?>.</b></p>
						</div>

					</div>
				</div>

				<!-- ÁREA LOGOS E PROJETOS -->
				<div class="area-projetos" id="area-projetos" style="display: none;">
					<div class="row">

						<div class="col-md-6 area-esquerda">

							<p><?php echo rwmb_meta('Palupa_servico_rodape'); ?></p>

							<!-- CONTAGEM -->
							<span><b class="stats-number" data-stop="<?php echo rwmb_meta('Palupa_servico_rodape_porcentagem_esquerdo'); ?>"></b>%<small><span style="width:<?php echo rwmb_meta('Palupa_servico_rodape_porcentagem_esquerdo')?>%;"  class="barraPorcento1" data-stop="<?php echo rwmb_meta('Palupa_servico_rodape_porcentagem_esquerdo'); ?>"></span></small></span>

							<!-- LOGOS -->
							<div class="logos">
								<?php
								$fotos_esquerda_logo = rwmb_meta('Palupa_cliente_logo_rodape_esquerdo');

								foreach ($fotos_esquerda_logo as $fotos_esquerda_logo => $foto_logo) {


									?>
									<img src="<?php echo $foto_logo['url'] ?>" alt="" class="img-responsive">
									<?php } ?>
								</div>

							</div>

							<div class="col-md-6 area-direita">

								<p><?php echo rwmb_meta('Palupa_servico_rodape2'); ?> </p>

								<!-- CONTAGEM -->
								<span><b class="stats-number" data-stop="<?php echo rwmb_meta('Palupa_servico_rodape_porcentagem_direito'); ?>"></b>%<small><span style="width:<?php echo rwmb_meta('Palupa_servico_rodape_porcentagem_direito')?>%;" class="barraPorcento" data-stop="<?php echo rwmb_meta('Palupa_servico_rodape_porcentagem_direito'); ?>"></span></small></span>

								<!-- LOGOS -->
								<div class="logos">
									<?php
									$fotos_direita_logo = rwmb_meta('Palupa_cliente_logo_rodape_direito');

									foreach ($fotos_direita_logo as $fotos_direita_logo => $foto_logo_direito) {


										?>
										<img src="<?php echo $foto_logo_direito['url'] ?>" alt="" class="img-responsive">
										<?php } ?>
									</div>

								</div>

							</div>
						</div>

						<!-- LINK PARA VOLTAR A PÁGINA -->
						<a href="<?php echo home_url('/servicos'); ?>" title="Voltar para a página de serviços" class="voltar"><img  src="<?php bloginfo('template_directory'); ?>/img/arrowLeftRosa.svg" alt="">Voltar</a>

					</section>

	</div>

	<script>
		/*
			CONTADOR NUMERAL SERVICO
		*/
		$(document).ready(function() {

			$.fn.isOnScreen = function(){
				var viewport = {};
				viewport.top = $(window).scrollTop();
				viewport.bottom = viewport.top + $(window).height();
				var bounds = {};
				bounds.top = this.offset().top;
				bounds.bottom = bounds.top + this.outerHeight();
				return ((bounds.top <= viewport.bottom) && (bounds.bottom >= viewport.top));
			};

			var testeMostrarVisita = false;
			$( window ).scroll(function() {
				if ($('.area-sobre-servico').isOnScreen() == true && testeMostrarVisita == false) {

					$("span.number").each(function() {
		 				var t = $(this);
		 				$({
		 					Counter: 0
		 				}).animate({
		 					Counter: t.attr("data-stop")
		 				}, {
		 					duration: 5e3,
		 					easing: "swing",
		 					step: function(a) {
		 						t.text(Math.ceil(a))
		 					}
		 				})
		 			});
					testeMostrarVisita = true;
				};
			});
		});

		/*
			CONTADOR NUMERAL CIRCULO SVG
		*/
		$(document).ready(function() {

			$.fn.isOnScreen = function(){
				var viewport = {};
				viewport.top = $(window).scrollTop();
				viewport.bottom = viewport.top + $(window).height();
				var bounds = {};
				bounds.top = this.offset().top;
				bounds.bottom = bounds.top + this.outerHeight();
				return ((bounds.top <= viewport.bottom) && (bounds.bottom >= viewport.top));
			};

			var testeMostrarSvg = false;
			$( window ).scroll(function() {
				if ($('#porcentagemValorCirculo').isOnScreen() == true && testeMostrarSvg == false) {

					var cem = 629;
					var zero = 1;

					valorPorcentagem = $("#valorCirculo").attr('data-valorPorcentagem');

					valorPorcentagem = (valorPorcentagem * 629) / 100;

					$('#porcentagemValorCirculo path').css('stroke-dashoffset',valorPorcentagem);
					$('#porcentagemValorCirculo path').css('-webkit-animation','loadSvg 5.3s');
					$('#porcentagemValorCirculo path').css('-moz-animation','loadSvg 5.3s');
					$('#porcentagemValorCirculo path').css('-o-animation','loadSvg 5.3s');
					$('#porcentagemValorCirculo path').css('animation','loadSvg 5.3s');
					$(".valorPorcentagem").each(function() {
		 				var t = $(this);
		 				$({
		 					Counter: 0
		 				}).animate({
		 					Counter: t.attr("data-stop")
		 				}, {
		 					duration: 5e3,
		 					easing: "swing",
		 					step: function(a) {
		 						t.text(Math.ceil(a))
		 					}
		 				})
		 			});
					testeMostrarSvg = true;
				};
			});
		});

		/*
			CONTADOR NUMERAL E BARRA RODAPE SERVICOS
		*/
		$(document).ready(function() {

			$.fn.isOnScreen = function(){
				var viewport = {};
				viewport.top = $(window).scrollTop();
				viewport.bottom = viewport.top + $(window).height();
				var bounds = {};
				bounds.top = this.offset().top;
				bounds.bottom = bounds.top + this.outerHeight();
				return ((bounds.top <= viewport.bottom) && (bounds.bottom >= viewport.top));
			};

			var testeMostrarNum = false;
			$( window ).scroll(function() {
				if ($('#area-projetos').isOnScreen() == true && testeMostrarNum == false) {


					$(".stats-number").each(function() {
			           var t = $(this);
			           $({
			               Counter: 0
			           }).animate({
			               Counter: t.attr("data-stop")
			           }, {
			               duration: 5e3,
			               easing: "swing",
			               step: function(a) {
			                   t.text(Math.ceil(a))
			               }
			           })
			      	});

			      	$(".barraPorcento1").each(function() {
			           var barraA = $(this);
			           $({
			               Counter: 0
			           }).animate({
			               Counter: barraA.attr("data-stop")
			           }, {
			               duration: 5e3,
			               easing: "swing",
			               step: function(a) {
			                   barraA.css({"width": Math.ceil(a)+"%"})
			               }
			           })
			      	});

			      	$(".barraPorcento").each(function() {
			           var barraB = $(this);
			           $({
			               Counter: 0
			           }).animate({
			               Counter: barraB.attr("data-stop")
			           }, {
			               duration: 5e3,
			               easing: "swing",
			               step: function(a) {
			                   barraB.css({"width": Math.ceil(a)+"%"})
			               }
			           })
			      	});

			      	testeMostrarNum = true;
				};
			});
		});

			/*
			CONTADOR NUMERAL E BARRA RODAPE SERVICOS
		*/
		$(document).ready(function() {

			$.fn.isOnScreen = function(){
				var viewport = {};
				viewport.top = $(window).scrollTop();
				viewport.bottom = viewport.top + $(window).height();
				var bounds = {};
				bounds.top = this.offset().top;
				bounds.bottom = bounds.top + this.outerHeight();
				return ((bounds.top <= viewport.bottom) && (bounds.bottom >= viewport.top));
			};

			var numQuemSomos = false;
			$( window ).scroll(function() {
				if ($('#area-projetos').isOnScreen() == true && numQuemSomos == false) {


					$(".stats-number").each(function() {
			           var t = $(this);
			           $({
			               Counter: 0
			           }).animate({
			               Counter: t.attr("data-stop")
			           }, {
			               duration: 5e3,
			               easing: "swing",
			               step: function(a) {
			                   t.text(Math.ceil(a))
			               }
			           })
			      	});

			      	numQuemSomos = true;
				};
			});
		});

	</script>
<?php

get_footer();
