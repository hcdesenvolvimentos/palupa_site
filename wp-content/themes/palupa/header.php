<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package palupa
 */
	global $configuracao;


	$facebook   = $configuracao['opt-facebook'];
	$twitter    = $configuracao['opt-twitter'];
	$instagram  = $configuracao['opt-instagram'];
	$googlePlus = $configuracao['opt-gplus'];
	$linkedin   = $configuracao['opt-linkedin'];
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>

<!-- FAVICON -->
<link rel="shortcut icon" type="image/x-icon" href=" <?php echo get_template_directory_uri(); ?>/favicon.ico">

</head>

<body <?php body_class(); ?>>

<?php if(is_front_page()): ?>
	<!-- LOADER -->
	<div class="areaLoader">
		<div class="loader">
			<span class="icon-palupa"></span>
			<div class="slidingVertical">
				<?php
				$arrayFrases = array("Alimentando unicórnios...", "Servindo o café...", "Fazendo flexões...", "Observando o universo...", "Contando as galáxias...", "Consertando o Super Nintendo...", "Zerando Super Mario Bros...", "Cronometrando o tempo da cafeteira...", "Resolvendo equações de segundo grau...", "Pedindo a pizza...", "Estacionando cavalos-marinhos...", "Trabalhando na máquina do tempo...", "Criando nova versão do protótipo...", "Contando os mafagafinhos do ninho...", "Vestindo a armadura para a batalha...", "Sendo entrevistado pelo Jô...", "Plantando sementes coloridas..", "Batendo o recorde...", "Planejando a nova startup...", "Equipando os guerreiros...", "Fazendo brainstorm com Steve Jobs...", "Indo além...", "Aprendendo com os melhores" );
				$randomFrases = array_rand($arrayFrases, 4);
				foreach ($randomFrases as $randomFrase) {
					echo '<p>'.$arrayFrases[$randomFrase].'</p>';
				}
				?>
			</div>
		</div>
	</div>
<?php endif; ?>

	<!-- HEADER -->
	<header class="topo">

		<!-- BTN MENU TOPO -->
		<div id="areaMenuTopo" class="areaMenuTopo">

			<!-- ÁREA BTN ABRIR / FECHAR -->
			<button class="btnAbrirMenuTopo" id="btnAbrirMenuTopo">
				<span class="areaBars">
				<span class="bar bar1"></span>
				<span class="bar bar2"></span>
				<span class="bar bar3"></span>
				<span class="bar bar4"></span>
				</span>
			</button>

			<!-- ÁREA LOGO MENU -->
			<div class="areaLogoMenu">
				<a href="<?php echo home_url(''); ?>" title="Página inicial"><i class="icon-palupa" id="palupaIcone"></i></a>
			</div>
			<p id="tituloMenuHeader">Menu</p>

			<!-- ÁREA LOGO PALUPA -->
			<div class="areaLogoPalupa">
				<a href="<?php echo home_url(''); ?>" title="Página inicial">
					<h1>Palupa Marketing & Dev</h1>
				</a>
			</div>

			<!-- OPÇÕES DO MENU -->
			<nav class="menuPrincipal">
				<ul class="listaMenu">
					<li class="menuItem"><a href="<?php echo home_url('/clientes'); ?>" title="Clientes">Clientes</a></li>
					<li class="menuItem temSubMenu">
						<a href="<?php echo home_url('/servicos'); ?>" title="Serviços">Serviços</a>
						<ul class="subMenu">
							<?php
							$categoriaServicoItems = get_terms( array( 'taxonomy' => 'categoriaServico', 'hide_empty' => false, ) );

							foreach ($categoriaServicoItems as $categoriaServicoItem):
							?>
							<li><a href="<?php echo get_term_link($categoriaServicoItem, 'categoriaServico') ?>" title="<?php echo $categoriaServicoItem->name ?>"><?php echo $categoriaServicoItem->name ?></a></li>
							<?php endforeach; ?>
						</ul>
					</li>
					<li class="menuItem"><a href="<?php echo home_url('/quem-somos'); ?>" title="Quem Somos">Quem Somos</a></li>
					<li class="menuItem"><a href="<?php echo home_url('/blog'); ?>" title="Blog">Blog</a></li>
					<li class="menuItem"><a href="<?php echo home_url('/contato'); ?>" title="Contato">Contato</a></li>
					<li class="menuItem">
						<ul class="listaRedesSociais">
							<?php if(isset($facebook) &&!empty($facebook)): ?><li><a href="<?php echo $facebook; ?>" title="Facebook"><i class="fa fa-facebook"></i></a></li><?php endif; ?>
							<?php if(isset($twiiter) &&!empty($twiiter)): ?><li><a href="<?php echo $twitter; ?>" title="Twitter"><i class="fa fa-twitter"></i></a></li><?php endif; ?>
							<?php if(isset($instagram) &&!empty($instagram)): ?><li><a href="<?php echo $instagram; ?>" title="Instagram"><i class="fa fa-instagram"></i></a></li><?php endif; ?>
							<?php if(isset($googlePlus) &&!empty($googlePlus)): ?><li><a href="<?php echo $googlePlus; ?>" title="Google plus"><i class="fa fa-google-plus"></i></a></li><?php endif; ?>
							<?php if(isset($linkedin) &&!empty($linkedin)): ?><li><a href="<?php echo $linkedin; ?>" title="Linkedin"><i class="fa fa-linkedin"></i></a></li><?php endif; ?>
						</ul>
					</li>
				</ul>
			</nav>

		</div>

	</header>