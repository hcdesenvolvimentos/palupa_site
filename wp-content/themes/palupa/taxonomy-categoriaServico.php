<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package palupa
 */
global $post;

get_header(); ?>
<?php
$taxonomyDados = get_term_by('slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
$subTitulo = get_term_meta( $taxonomyDados->term_id, 'subTitulo', true );
$taxonomiaSlug = $taxonomyDados->slug;
?>
	<!-- PÁGINA DE SERVIÇO DETALHADO-->
	<div class="pg pg-servico-detalhado">

		<!-- TÍTULO PÁGINA -->
		<div class="tituloPagina">
			<span>Serviço</span>
		</div>

		<!-- BTN MENU SERVIÇO -->
		<div class="areaBtnMenuCliente">
			<button class="navTrigger navTriggerServicos" id="btnAbrirMenuLateral" ><i class="fa fa-th"></i></button>
			<button class="navTrigger navTriggerServicos" id="btnFecharMenuLateral"><i class="fa fa-times"></i></button>
			<p id="tituloMenuInternas">Serviços</p>
		</div>

		<!-- MENU SERVIÇO -->
		<nav class="menu-lateral menu-lateralServicos">

			<ul class="listaMenuLateral">
				<!-- LOOP DE SERVIÇOS / SIDEBAR -->
				<?php
				$sidebarServicos = new WP_Query(array(
					'post_type' => 'servico',
					'posts_per_page' 	=> -1,
					'tax_query' 		=> array(
						array(
							'taxonomy' => 'categoriaServico',
							'field'    => 'slug',
							'terms'    => $taxonomiaSlug,
							)
						)
					)
				);
				while ( $sidebarServicos->have_posts() ) : $sidebarServicos->the_post();


				?>
				<li class="menu-item"><a href="<?php echo get_permalink(); ?>" title="<?php echo get_the_title() ?>"><?php echo get_the_title() ?></a></li>
				<?php endwhile; wp_reset_query(); ?>
			</ul>

		</nav>

		<!-- ANIMAÇÃO TOPO -->
		<div class="areaAnimacaoTopo">
			<!-- INFORMAÇÕES PRINCIPAIS TOPO SERVIÇOS  -->

			<section class="info-servico" style="background:url(<?php echo get_term_meta( $taxonomyDados->term_id, 'bgTopo', true ); ?>)no-repeat!important;background-position: center!important; background-size: cover!important;">

				<div class="lente">
					<ul>
						<li class="lista-texto">
							<div class="area-texto-servico">
								<h4><?php echo $taxonomyDados->name; ?></h4>
								<b><?php echo $subTitulo; ?></b>
								<?php echo $taxonomyDados->description; ?>

							</div>
						</li>
						<li class="lista-check">
							<div class="descricoes">
								<!-- LOOP ITENS DA LISTA-->
								<?php
								$servicos = new WP_Query(array(
									'post_type' => 'servico',
									'posts_per_page' 	=> -1,
									'tax_query' 		=> array(
										array(
											'taxonomy' => 'categoriaServico',
											'field'    => 'slug',
											'terms'    => $taxonomiaSlug,
											)
										)
									)
								);
								while ( $servicos->have_posts() ) : $servicos->the_post();
								?>

									<a href="<?php echo get_permalink(); ?>" title="<?php echo the_title(); ?>">
										<p><b><?php echo the_title(); ?>:</b> <?php customExcerpt(140); ?> <span class="spanVerMais">ver mais</span> <img class="setinha" src="<?php bloginfo('template_directory'); ?>/img/seta.png" alt=""></p>
									</a>

								<?php endwhile;wp_reset_query(); ?>

							</div>
						</li>
					</ul>
				</div>

			</section>
		</div>
<?php

get_footer();
