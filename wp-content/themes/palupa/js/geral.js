
	$(function(){



		/*
			 SCROLL TOPO ANTES DE CARREGAR A PÁGINA
		 */
		window.onbeforeunload = function(){
			window.scrollTo(0,0);
		}

		/* Smooth scroll */
		// var url          = window.location.pathname;
		// var urlArray     = url.split('/');
		// var nomeDaPagina = urlArray[urlArray.length-2];
		// if (nomeDaPagina != "clientes") {
		// 	Math.easeOutQuad = function (t, b, c, d) { t /= d; return -c * t*(t-2) + b; };
		// 	(function() {
		// 		var
		// 		interval,
		// 		mult = 0,
		// 		dir = 0,
		// 		steps = 40,
		// 		length = 30;
		// 		function MouseWheelHandler(e) {
		// 			e.preventDefault();
		// 			clearInterval(interval);
		// 			++mult;
		// 			var delta = -Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
		// 			if(dir!=delta) {
		// 				mult = 1;
		// 				dir = delta;
		// 			}

		// 			for(var tgt=e.target; tgt!=document.documentElement; tgt=tgt.parentNode) {
		// 				var oldScroll = tgt.scrollTop;
		// 				tgt.scrollTop+= delta;
		// 				if(oldScroll!=tgt.scrollTop) break;

		// 			}
		// 			var start = tgt.scrollTop;
		// 			var end = start + length*mult*delta;
		// 			var change = end - start;
		// 			var step = 0;
		// 			interval = setInterval(function() {
		// 				var pos = Math.easeOutQuad(step++,start,change,steps);
		// 				tgt.scrollTop = pos;
		// 				if(step>=steps) {
		// 					mult = 0;
		// 					clearInterval(interval);
		// 				}
		// 			},10);
		// 		}

		// 	window.addEventListener("mousewheel", MouseWheelHandler, false);
		// 	window.addEventListener("DOMMouseScroll", MouseWheelHandler, false);
		// 	})();
		// }

		/*
			SEGUNDOS ANTES DE IR PARA O ENDEREÇO DO LINK
		 */
		 $('a[href]:not([href^="mailto\\:"], [href^="tel\\:"], [target="_blank"], [href*="#"],.fancybox)').click(function (e) {
		 	e.preventDefault();
		 	$("html, body").animate({ scrollTop: 0 }, "slow");

		 	var goTo = this.getAttribute("href");

		 	setTimeout(function(){
		 		$('body').addClass('beforeLoad');
		 	},500);

		 	setTimeout(function(){
		 		window.location = goTo;
		 	},1000);
		 });

		/*
			PAGE LOADER
		 */
		$(window).load(function(){
			setTimeout(function(){
				$('body').addClass('loaded');
			}, 1000);
		});

		/*
			CARROSSEL DE CLIENTES INICIAL
		*/
		$(document).ready(function() {

		 	$("#carrosselClientes").owlCarousel({
		 		responsiveClass: true,
		 		responsive: {
		 			0: {
		 				items: 1,
		 			},
		 			500: {
		 				items: 3,
		 			},
		 			900: {
		 				items: 5,
		 			},
		 			1000: {
		 				items: 7,
		 			}
		 		},
		 		center:true,
		 		loop: true,
		 		mouseDrag: true,
		 		autoplay:true,
		 		autoplayTimeout:31,
		 		smartSpeed:7500,
		 	});

		});


		/*
			CARROSSEL QUEM SOMOS
		 */
		$(document).ready(function(){


			var goClick = true;
			var dotAtual = 0;
			var transitionEnd = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

			$(".nav-dot").click(function(e){
				if (goClick){
					goClick = false;

					$(".nav-dot").css('pointer-events', 'none');
					var dotItem = $(this);
					var dotIndex = $('.nav-dot').index(dotItem);

					if (dotIndex != dotAtual) {
						if(dotIndex > dotAtual){
		                    stateIn = "stateInRight";
		                    stateOut = "stateOutLeft";
		                }
		                else{
		                    stateIn = "stateInLeft";
		                    stateOut = "stateOutRight";
		                }
		                var slide = '#' + $(this).attr('data-slide');
		                var slideItem = $(slide);
		                var slideIndex = $(".slide").index(slideItem);
		                var slideIndexPrev = '#slide-' + (slideIndex - 1);
		                var slideIndexNext = '#slide-' + (slideIndex + 1);

						$('.slide').filter('.active').removeClass("major").addClass(stateOut).one(transitionEnd, function() {
							$(this).removeClass(stateOut + ' active');
						});


						$('.slide').eq(slideIndex).addClass(stateIn).addClass("major").one(transitionEnd, function() {
							$(this).removeClass(stateIn).addClass("active");
							goClick = true;
							$('.nav-dot').delay( 2000 ).css('pointer-events', 'initial');
						});

						$('.nav-dots').find(".nav-dot").filter('.active').removeClass('active');
						dotItem.addClass("active");
					}

					dotAtual = dotIndex;
				}
			});

		});

		/*
			BOTÃO MENU PRINCIPAL
		 */
		$(document).ready(function(){

			$("#btnAbrirMenuTopo").click(function(e){
				if (!$("#btnAbrirMenuTopo").hasClass('btnFechar')) {
					e.preventDefault();
					$("#areaMenuTopo").addClass('aberto mostrarMenu');
					$("#btnAbrirMenuTopo").addClass('btnFechar');
				} else{
					e.preventDefault();
					$("#areaMenuTopo").removeClass('aberto mostrarMenu');
					$("#btnAbrirMenuTopo").removeClass('btnFechar');
				}
			});

			$("#btnAbrirMenuTopo").mouseenter(function(e) {
				$("#palupaIcone").addClass("hover");
			});

			$("#btnAbrirMenuTopo").mouseleave(function(e) {
				$("#palupaIcone").removeClass("hover");
			});

			$(".menuPrincipal .listaMenu .menuItem a").click(function(e){
				$("#areaMenuTopo").removeClass('aberto mostrarMenu');
				$("#btnAbrirMenuTopo").removeClass('btnFechar');
			});

		});

		/*
			BANNER CLIENTE
		 */
		$(document).ready(function(){

			var div = $('#bannerCliente'),
			divHeight = div.height(),
			scroll;
			var check = false;
			(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);

			if (check == true) {
				$("#bannerCliente").height(115);
			}else{
				$(window).scroll(function () {
					scroll = $(this).scrollTop();
					div.height(divHeight - scroll);

					console.log(div.height());
					// if (divHeight - scroll >= 115) {
					// 	$('.animacaoCLienteDetalhado').removeClass('areaAnimacaoTopo');
					// }
				});
			}

		});

		/*
			MENU CLIENTES
		 */
		$(document).ready(function(){

			$(".navTriggerClientes").click(function(e){
				e.preventDefault();
				if ($('.menu-lateralClientes').is(".aberto")) {
					$('.menu-lateralClientes').removeClass("aberto");
					$('#btnAbrirMenuLateral').show();
					$('#btnFecharMenuLateral').hide();

				}else{
					$('.menu-lateralClientes').addClass("aberto");
					$('#btnFecharMenuLateral').show();
					$('#btnAbrirMenuLateral').hide();
				}
			});

			$(".menu-lateralClientes ul li a").click(function(e){
				$('.menu-lateralClientes').removeClass("aberto");
				$('#btnAbrirMenuLateral').show();
				$('#btnFecharMenuLateral').hide();
			});

		});

		/*
			CARROSSEL DE DEPOIMENTOS SERVICO
		*/
		$(document).ready(function() {

			$("#carrosselParceirosItem").owlCarousel({
		 		items : 7,
		 		dots: true,
		 		loop: true,
		 		lazyLoad: true,
		 		mouseDrag: true,
		 		autoplay:true,
		 		autoplayTimeout:5000,
		 		autoplayHoverPause:true,
		 		animateOut: 'fadeOut',
		 		smartSpeed: 450,
		 		autoplaySpeed: 4000,
		 			responsiveClass:true,
		        responsive:{
		        	0:{
		        		items:1
		        	},
		        	500:{
		        		items:3
		        	},
		            800:{
		                items:4
		            },
		            1200:{
		                items:7
		            }
		        }	   
		 	});
		$("#carrosselClientesItem").owlCarousel({
		 		items : 7,
		 		dots: true,
		 		loop: true,
		 		lazyLoad: true,
		 		mouseDrag: true,
		 		autoplay:true,
		 		autoplayTimeout:5000,
		 		autoplayHoverPause:true,
		 		animateOut: 'fadeOut',
		 		smartSpeed: 450,
		 		autoplaySpeed: 4000,
		 			responsiveClass:true,
		        responsive:{
		        	0:{
		        		items:1
		        	},
		        	500:{
		        		items:3
		        	},
		            800:{
		                items:4
		            },
		            1200:{
		                items:7
		            }
		        }	   
		 	});

		 	$("#carrossel-depoimentos").owlCarousel({
		 		items : 1,
		 		dots: true,
		 		loop: false,
		 		lazyLoad: true,
		 		mouseDrag: true,
		 		autoplay:true,
		 		autoplayTimeout:7000,
		 		autoplayHoverPause:true,
		 		animateOut: 'fadeOut',
		 		smartSpeed: 450,
		 		autoplaySpeed: 5000,
		 	});
		 	var carrossel_destaque = $("#carrossel-depoimentos").data('owlCarousel');
		 	$('.navegacaoDepoimentoFrent').click(function(){ carrossel_destaque.prev(); });
		 	$('.navegacaoDepoimentoTras').click(function(){ carrossel_destaque.next(); });


	 		$("#carrosselNoticiasPG").owlCarousel({
		 		items : 1,
		 		dots: true,
		 		loop: false,
		 		lazyLoad: true,
		 		mouseDrag: true,
		 		autoplay:true,
		 		autoplayTimeout:5000,
		 		autoplayHoverPause:true,
		 		animateOut: 'fadeOut',
		 		smartSpeed: 450,
		 		autoplaySpeed: 4000,
		 	});
		 	var carrossel_bLOG = $("#carrosselNoticiasPG").data('owlCarousel');
		 	$('.carrosselPGNoticiasBtnF').click(function(){ carrossel_bLOG.prev(); });
		 	$('.carrosselPGNoticiasBtnT').click(function(){ carrossel_bLOG.next(); });
		});


		/*
			MENU SERVICOS
		*/
		$(document).ready(function() {

			$(".navTriggerServicos").click(function(e){
				e.preventDefault();
				if ($('.menu-lateralServicos').is(".aberto")) {
					$('.menu-lateralServicos').removeClass("aberto");
					$('#btnAbrirMenuLateral').show();
					$('#btnFecharMenuLateral').hide();

				}else{
					$('.menu-lateralServicos').addClass("aberto");
					$('#btnFecharMenuLateral').show();
					$('#btnAbrirMenuLateral').hide();
				}
			});

			$(".menu-lateralServicos ul li a").click(function(e){
				$('.menu-lateralServicos').removeClass("aberto");
				$('#btnAbrirMenuLateral').show();
				$('#btnFecharMenuLateral').hide();
			});

	 	});

		$("#carrossel-imagens-case").owlCarousel({
	        items : 1,
	        nav: false,
	        // loop: true,
	        lazyLoad: true,
	        mouseDrag: true,
	        autoplay:true,
		    autoplayTimeout:10000,
		    autoplayHoverPause:true,
		    animateOut: 'fadeOut',
		    smartSpeed: 450,
		    autoplaySpeed: 4000,

	    });



	   var carrosselimgproj = $("#carrossel-imagens-case").data('owlCarousel');
	   $('.navegacaoCarrosselProjeTras').click(function(){ carrosselimgproj.prev(); });
	   $('.navegacaoCarrosselProjeFrente').click(function(){ carrosselimgproj.next(); });

		$(document).ready(function() {
			$(".fancybox").fancybox({
				openEffect	: 'none',
				closeEffect	: 'none'
			});
		});
	});