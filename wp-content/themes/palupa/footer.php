<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package palupa
 */
	global $configuracao;

	$facebook   = $configuracao['opt-facebook'];
	$twitter    = $configuracao['opt-twitter'];
	$instagram  = $configuracao['opt-instagram'];
	$googlePlus = $configuracao['opt-gplus'];
	$linkedin   = $configuracao['opt-linkedin'];
	$email      = $configuracao['opt-email'];
	$telefone1  = $configuracao['opt-telefone1'];
	$telefone2  = $configuracao['opt-telefone2'];
	$endereco   = $configuracao['opt-endereco'];
	$endereco2   = $configuracao['opt-endereco2'];
	$geo        = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($endereco).'&sensor=false');
	$geo        = json_decode($geo, true);
	$enderecoGoogle = 'https://www.google.com.br/maps/place/' . urldecode($endereco);
	$enderecoGoogle2 = 'https://www.google.com.br/maps/place/' . urldecode($endereco2);

	if ($geo['status'] == 'OK') {
		$latitude = $geo['results'][0]['geometry']['location']['lat'];
		$longitude = $geo['results'][0]['geometry']['location']['lng'];
	}
?>
	<!-- MODAL DE CONTATO -->
	<div class="modal fade bd-example-modal-lg" id="modalContato" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<!-- PÁGINA CONTATO -->
					<div class="pg pg-contato">

						<!-- ANIMAÇÃO RODAPÉ -->
						<div class="areaAnimacaoRodape">

							<!-- ÁREA TXT / FORMULÁRIO -->
							<div class="areaTxtForm">

								<div class="containerForm">

									<div class="row">
										<div class="col-md-12">
											<!-- ÁREA FORM CONTATO -->
											<div class="areaFormContato">
												<strong>Contato</strong>
												<div class="form">

													<?php echo do_shortcode('[contact-form-7 id="47" title="Formulário de contato" html_name="contato-modal"]'); ?>

												</div>
											</div>

										</div>
									</div>

								</div>

							</div>

						</div>

					</div>
			</div>
		</div>
	</div>

	<!-- MODAL CONTATO SUCESSO -->
	<div class="modal" id="modalContatoSucesso" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content modalContatoSucesso">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 345.834 345.834" xml:space="preserve">
					<g>
						<path d="M339.798,260.429c0.13-0.026,0.257-0.061,0.385-0.094c0.109-0.028,0.219-0.051,0.326-0.084   c0.125-0.038,0.247-0.085,0.369-0.129c0.108-0.039,0.217-0.074,0.324-0.119c0.115-0.048,0.226-0.104,0.338-0.157   c0.109-0.052,0.22-0.1,0.327-0.158c0.107-0.057,0.208-0.122,0.312-0.184c0.107-0.064,0.215-0.124,0.319-0.194   c0.111-0.074,0.214-0.156,0.321-0.236c0.09-0.067,0.182-0.13,0.27-0.202c0.162-0.133,0.316-0.275,0.466-0.421   c0.027-0.026,0.056-0.048,0.083-0.075c0.028-0.028,0.052-0.059,0.079-0.088c0.144-0.148,0.284-0.3,0.416-0.46   c0.077-0.094,0.144-0.192,0.216-0.289c0.074-0.1,0.152-0.197,0.221-0.301c0.074-0.111,0.139-0.226,0.207-0.34   c0.057-0.096,0.118-0.19,0.171-0.289c0.062-0.115,0.114-0.234,0.169-0.351c0.049-0.104,0.101-0.207,0.146-0.314   c0.048-0.115,0.086-0.232,0.128-0.349c0.041-0.114,0.085-0.227,0.12-0.343c0.036-0.118,0.062-0.238,0.092-0.358   c0.029-0.118,0.063-0.234,0.086-0.353c0.028-0.141,0.045-0.283,0.065-0.425c0.014-0.1,0.033-0.199,0.043-0.3   c0.025-0.249,0.038-0.498,0.038-0.748V92.76c0-4.143-3.357-7.5-7.5-7.5h-236.25c-0.066,0-0.13,0.008-0.196,0.01   c-0.143,0.004-0.285,0.01-0.427,0.022c-0.113,0.009-0.225,0.022-0.337,0.037c-0.128,0.016-0.255,0.035-0.382,0.058   c-0.119,0.021-0.237,0.046-0.354,0.073c-0.119,0.028-0.238,0.058-0.356,0.092c-0.117,0.033-0.232,0.069-0.346,0.107   c-0.117,0.04-0.234,0.082-0.349,0.128c-0.109,0.043-0.216,0.087-0.322,0.135c-0.118,0.053-0.235,0.11-0.351,0.169   c-0.099,0.051-0.196,0.103-0.292,0.158c-0.116,0.066-0.23,0.136-0.343,0.208c-0.093,0.06-0.184,0.122-0.274,0.185   c-0.106,0.075-0.211,0.153-0.314,0.235c-0.094,0.075-0.186,0.152-0.277,0.231c-0.09,0.079-0.179,0.158-0.266,0.242   c-0.099,0.095-0.194,0.194-0.288,0.294c-0.047,0.05-0.097,0.094-0.142,0.145c-0.027,0.03-0.048,0.063-0.074,0.093   c-0.094,0.109-0.182,0.223-0.27,0.338c-0.064,0.084-0.13,0.168-0.19,0.254c-0.078,0.112-0.15,0.227-0.222,0.343   c-0.059,0.095-0.12,0.189-0.174,0.286c-0.063,0.112-0.118,0.227-0.175,0.342c-0.052,0.105-0.106,0.21-0.153,0.317   c-0.049,0.113-0.092,0.23-0.135,0.345c-0.043,0.113-0.087,0.225-0.124,0.339c-0.037,0.115-0.067,0.232-0.099,0.349   c-0.032,0.12-0.066,0.239-0.093,0.36c-0.025,0.113-0.042,0.228-0.062,0.342c-0.022,0.13-0.044,0.26-0.06,0.39   c-0.013,0.108-0.019,0.218-0.027,0.328c-0.01,0.14-0.019,0.28-0.021,0.421c-0.001,0.041-0.006,0.081-0.006,0.122v46.252   c0,4.143,3.357,7.5,7.5,7.5s7.5-3.357,7.5-7.5v-29.595l66.681,59.037c-0.348,0.245-0.683,0.516-0.995,0.827l-65.687,65.687v-49.288   c0-4.143-3.357-7.5-7.5-7.5s-7.5,3.357-7.5,7.5v9.164h-38.75c-4.143,0-7.5,3.357-7.5,7.5s3.357,7.5,7.5,7.5h38.75v43.231   c0,4.143,3.357,7.5,7.5,7.5h236.25c0.247,0,0.494-0.013,0.74-0.037c0.115-0.011,0.226-0.033,0.339-0.049   C339.542,260.469,339.67,260.454,339.798,260.429z M330.834,234.967l-65.688-65.687c-0.042-0.042-0.087-0.077-0.13-0.117   l49.383-41.897c3.158-2.68,3.546-7.412,0.866-10.571c-2.678-3.157-7.41-3.547-10.571-0.866l-84.381,71.59l-98.444-87.158h208.965   V234.967z M185.878,179.888c0.535-0.535,0.969-1.131,1.308-1.765l28.051,24.835c1.418,1.255,3.194,1.885,4.972,1.885   c1.726,0,3.451-0.593,4.853-1.781l28.587-24.254c0.26,0.38,0.553,0.743,0.89,1.08l65.687,65.687H120.191L185.878,179.888z"></path>
						<path d="M7.5,170.676h126.667c4.143,0,7.5-3.357,7.5-7.5s-3.357-7.5-7.5-7.5H7.5c-4.143,0-7.5,3.357-7.5,7.5   S3.357,170.676,7.5,170.676z"></path>
						<path d="M20.625,129.345H77.5c4.143,0,7.5-3.357,7.5-7.5s-3.357-7.5-7.5-7.5H20.625c-4.143,0-7.5,3.357-7.5,7.5   S16.482,129.345,20.625,129.345z"></path>
						<path d="M62.5,226.51h-55c-4.143,0-7.5,3.357-7.5,7.5s3.357,7.5,7.5,7.5h55c4.143,0,7.5-3.357,7.5-7.5S66.643,226.51,62.5,226.51z"></path>
					</g>
				</svg>
				<strong>Sucesso!</strong>
				<p>Logo retornaremos o seu contato.</p>
				<button id="modalContatoSucessoFechar" class="btn btn-default" data-dismiss="modalContatoSucesso">Fechar</button>
			</div>
		</div>
	</div>

	<!-- MODAL CONTATO ERRO -->
	<div class="modal" id="modalContatoErro" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content modalContatoErro">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="554.625px" height="554.625px" viewBox="0 0 554.625 554.625" xml:space="preserve">
					<g>
						<g>
							<path d="M554.625,459l-76.5-130.05V133.875c0-21.038-17.213-38.25-38.25-38.25H38.25C17.212,95.625,0,112.837,0,133.875v229.5    c0,21.037,17.212,38.25,38.25,38.25h302.175L306,459H554.625z M453.263,120.487c3.825,3.825,5.737,7.65,5.737,13.388v162.562    l-28.688-47.812L388.237,321.3L306,248.625L453.263,120.487z M430.312,114.75l-191.25,166.388L47.812,114.75H430.312z     M24.862,376.763c-3.825-3.825-5.737-7.65-5.737-13.388v-229.5c0-5.737,1.913-9.562,5.737-13.388l147.263,128.138L24.862,376.763z     M47.812,382.5l139.612-120.487L239.062,306l51.638-43.987l87.975,76.5L351.9,382.5H47.812z M430.312,286.875l91.8,153H340.425    L430.312,286.875z"></path>
							<rect x="420.75" y="325.125" width="19.125" height="57.375"></rect>
							<rect x="420.75" y="401.625" width="19.125" height="19.125"></rect>
						</g>
					</g>
				</svg>
				<strong>Erro!</strong>
				<p>Houve um erro ao tentar enviar a sua mensagem. Verifique se todos os campos foram preenchidos corretamente.</p>
				<button id="modalContatoErroFechar" class="btn btn-default" data-dismiss="modalContatoErro">Fechar</button>
			</div>
		</div>
	</div>


	<!-- ANIMAÇÃO RODAPE -->
	<div class="areaAnimacaoRodape">
		<!-- RODAPÉ -->
		<footer class="rodape">

			<div class="row">

				<!-- ÁREA INFOMAÇÕES DE CONTATO -->
				<div class="col-md-6 areaInfoContato">

					<!-- INFORMAÇÕES DE CONTATO -->
					<div class="infoContato">
						<h3>Palupa </h3>
						<p>A GENTE NÃO FICA ONLINE. A GENTE VIVE ONLINE.</p>
						<?php if(isset($endereco) && !empty($endereco)): ?><a href="<?php echo $enderecoGoogle; ?>" title="Endereço da Palupa no google maps" target="_blank"><span><?php echo $endereco; ?></span></a><?php endif; ?>
						<?php if(isset($telefone1) && !empty($telefone1)): ?><a href="tel:+<?php echo preg_replace("/[^0-9]/", "", $telefone1); ?>" title="<?php echo $telefone1; ?>"><span><?php echo $telefone1; ?></span></a><?php endif; ?>
						<?php if(isset($endereco2) && !empty($endereco2)): ?><a href="<?php echo $enderecoGoogle2; ?>" title="Endereço da Palupa no google maps" target="_blank"><span style="margin-top: 20px;"><?php echo $endereco2; ?></span></a><?php endif; ?>
						<?php if(isset($telefone2) && !empty($telefone2)): ?><a href="tel:+<?php echo preg_replace("/[^0-9]/", "", $telefone2); ?>" title="<?php echo $telefone2; ?>"><span><?php echo $telefone2; ?></span></a><?php endif; ?>
						<?php if(isset($email) && !empty($email)): ?><a href="mailto:<?php echo $email; ?>" title="Endereço de e-mail da Palupa"><span style="margin-top: 20px;"><?php echo $email; ?></span></a><?php endif; ?>
						<ul class="redesSociaisRodape">
							<?php if(isset($facebook) && !empty($facebook)): ?><li><a href="<?php echo $facebook; ?>" title="Facebook"><i class="fa fa-facebook"></i></a></li><?php endif; ?>
							<?php if(isset($twiiter) && !empty($twiiter)): ?><li><a href="<?php echo $twitter; ?>" title="Twitter"><i class="fa fa-twitter"></i></a></li><?php endif; ?>
							<?php if(isset($instagram) && !empty($instagram)): ?><li><a href="<?php echo $instagram; ?>" title="Instagram"><i class="fa fa-instagram"></i></a></li><?php endif; ?>
							<?php if(isset($googlePlus) && !empty($googlePlus)): ?><li><a href="<?php echo $googlePlus; ?>" title="Google plus"><i class="fa fa-google-plus"></i></a></li><?php endif; ?>
							<?php if(isset($linkedin) && !empty($linkedin)): ?><li><a href="<?php echo $linkedin; ?>" title="Linkedin"><i class="fa fa-linkedin"></i></a></li><?php endif; ?>
						</ul>
							<!-- Large modal -->
					<button class="btn btn-primary btnRecebaPRoposta" data-toggle="modal" data-target=".bd-example-modal-lg">Receba uma proposta</button>

					</div>




				</div>

				<!-- ÁREA MAPA -->
				<div class="col-md-6 areaMapa">
					<div class="mapa" id="map"></div>
					<div class="noHoverMapa"></div>
					<div class="areaBtnMapa">
						<a href="<?php echo $enderecoGoogle; ?>" target="_blank" title="Palupa no Google Maps" class="btnMapa"></a>
					</div>
				</div>

			</div>

		</footer>
	</div>

	<!-- COPYRIGHT -->
	<div class="copyright text-center">
		<small>&copy; Copyright 2016 - <span>Palupa</span> Todos os direitos reservados.</small>
	</div>

	<script>
		var map;
		function initMap() {
			var palupa = {lat: <?php echo $latitude; ?>, lng: <?php echo $longitude; ?>};

			map = new google.maps.Map(document.getElementById('map'), {
				zoom: 13,
				center: palupa,

				styles: [{"elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#0F0919"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#d0e2e7"}]},{"elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#032d9e"}]},{"featureType":"poi.attraction","elementType":"geometry.fill","stylers":[{"color":"#b00308"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#e5e5e3"}]},{"featureType":"poi.business","elementType":"geometry.fill","stylers":[{"color":"#e8d608"}]},{"featureType":"poi.government","elementType":"geometry.fill","stylers":[{"color":"#b00308"}]},{"featureType":"poi.school","elementType":"geometry.fill","stylers":[{"color":"#b00308"}]},{"featureType":"transit.line","elementType":"geometry.fill","stylers":[{"saturation":-100}]}]
			});

			map.setOptions({draggable: false, zoomControl: false, scrollwheel: false, disableDoubleClickZoom: true});
		}
	</script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDYZ4riMiX1n78MsXnButSeGay_FlbJww&signed_in=true&callback=initMap"></script>

	<script>
		$(document).ready(function() {
			$('.wpcf7-submit').click(function(){
				function show_popup(){

					if($("div.wpcf7-validation-errors").length >= 1) {
						$("#modalContatoErro").show();
						clearInterval(robo);
					}
					if($("div.wpcf7-mail-sent-ok").length >= 1) {
						$("#modalContatoSucesso").show();
						clearInterval(robo);


					}
				};

				var robo = setInterval( show_popup, 500 );

			});

			$("#modalContatoErroFechar").click(function(){
				$("#modalContatoErro").hide();
			});

			$("#modalContatoSucessoFechar").click(function(){
				$("#modalContatoSucesso").hide();
				$("#modalContato").hide();
				$(".modal-backdrop").hide();

			});

		});
	</script>

<?php wp_footer(); ?>


</body>
</html>
