<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package palupa
 */
	global $post;

	$clientePost = $post;
	$bannerCliente = rwmb_meta('Palupa_cliente_imagem_fundo');
	$bannerCliente = array_shift(array_values($bannerCliente));
	$bannerCliente = $bannerCliente['full_url'];
	$arrayGaleriaCase = rwmb_meta("Palupa_cliente_galeria_case","type=image");

get_header(); ?>

	<!-- PÁGINA CLIENTE DETALHADO -->
	<div class="pg pg-cliente-detalhado">

		<div class="banner" id="bannerCliente" title="<?php echo get_the_title() ?>" style="background: url(<?php echo $bannerCliente; ?>)"></div>

		<section class="infoCliente">
			<div class="container">
				<div class="row">
					<div class="col-md-7">

						<?php
							$Palupa_cliente_depoimento = rwmb_meta('Palupa_cliente_depoimento');
							$cliente_depoimento_nome = rwmb_meta('Palupa_cliente_depoimento_nome');
							$cliente_listainfo = rwmb_meta('Palupa_cliente_listaInfo');
							// FOTO DO CLIENTE / DEPOIMENTO
							$arraylogoFotoCliente = rwmb_meta('Palupa_cliente_foto_depoimento','type=image');
							$UrllogoFotoCliente = array_values($arraylogoFotoCliente)[0];
							$fotCliente = $UrllogoFotoCliente['full_url'];
							// LOGO DO CLIENTE
							$arraylogoMarca = rwmb_meta('Palupa_cliente_logo','type=image');
							$UrllogoMarca = array_values($arraylogoMarca)[0];
							$logoMarca = $UrllogoMarca['full_url'];

						?>

						<!-- LOGO E NOME LOCAL -->
						<img src="<?php echo $logoMarca ?>" alt="<?php echo get_the_title() ?>" class="img-responsive">
						<div class="textoCliente">
							<p><?php echo get_the_content() ?></p>
						</div>

						<!-- ÁREA DEPOIMENTOS -->
						<div class="depoimentoCliente">
							<p><?php echo $Palupa_cliente_depoimento ?></p>
							<div class="fotoCliente" style="background:url(<?php echo $fotCliente ?>)"></div>
							<h2><?php echo $cliente_depoimento_nome  ?></h2>
						</div>


					</div>
					<div class="col-md-5">

						<div class="servicosCliente">
							<ul>
								<?php
									if(isset($cliente_listainfo) && !empty($cliente_listainfo))
										foreach ($cliente_listainfo as $item):
											$item      = explode("|", $item);
										$txtItem   = $item[0];
										$iconeItem = $item[1];
								?>
								<li>
									<img src="<?php echo $iconeItem; ?>" alt="<?php echo $txtItem; ?>" class="img-responsive">
									<h2><?php echo $txtItem; ?></h2>
								</li>
								<?php endforeach; ?>

							</ul>
						</div>

					</div>

				</div>
			</div>

		</section>

		<?php
		$ocultarCase = rwmb_meta('Palupa_servico_esconder');
		if ($ocultarCase == 0):
		$contadorPostagem = 0;
		$layout = 0;
		// LOOP DE PROJETOS
		$projetosPost = new WP_Query( array( 'post_type' => 'projeto', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );
		while ( $projetosPost->have_posts() ) : $projetosPost->the_post();
			$clienteID       = rwmb_meta('Palupa_projeto_cliente');
			$servicoID       = rwmb_meta('Palupa_projeto_servico');
			$mockupPrincipal = rwmb_meta('Palupa_projeto_imagem','type=image');
			$mockupPrincipal = array_values($mockupPrincipal)[0];
			$mockupPrincipal = $mockupPrincipal['full_url'];
			$mockupIpad      = rwmb_meta('Palupa_projeto_imagem2','type=image');
			$mockupIpad      = array_values($mockupIpad)[0];
			$mockupIpad      = $mockupIpad['full_url'];
			$tituloProjeto   = rwmb_meta('Palupa_projeto_nome_projeto');
			$linkTituloProjeto = preg_replace("/[\s_]/", "-", $tituloProjeto);
			$iconeProjeto    = rwmb_meta('Palupa_projeto_icone');
			$iconeProjeto = array_shift(array_values($iconeProjeto));
			$iconeProjeto = $iconeProjeto['full_url'];

			$chamada1        = rwmb_meta('Palupa_projeto_conteudo1_titulo');
			$chamada2        = rwmb_meta('Palupa_projeto_conteudo2_titulo');
			$chamada3        = rwmb_meta('Palupa_projeto_conteudo3_titulo');
			$txtChamada1     = rwmb_meta('Palupa_projeto_conteudo1_texto');
			$txtChamada2     = rwmb_meta('Palupa_projeto_conteudo2_texto');
			$txtChamada3     = rwmb_meta('Palupa_projeto_conteudo3_texto');
			$cor1            = rwmb_meta('Palupa_projeto_cor_logo1');
			$cor2            = rwmb_meta('Palupa_projeto_cor_logo2');
			$cor3            = rwmb_meta('Palupa_projeto_cor_logo3');
			$cor4            = rwmb_meta('Palupa_projeto_cor_logo4');
			if ( $clienteID == $clientePost->ID ):
				if ($servicoID != "153"):

				if($layout % 2 == 0):
		?>
		<script>
			$(document).ready(function(){
				$.fn.isOnScreen = function(){
					var viewport = {};
					viewport.top = $(window).scrollTop();
					viewport.bottom = viewport.top + $(window).height();
					var bounds = {};
					bounds.top = this.offset().top;
					bounds.bottom = bounds.top + this.outerHeight();
					return ((bounds.top <= viewport.bottom) && (bounds.bottom >= viewport.top));
				};

				$( window ).scroll(function() {
					if ($('#animar<?php echo $contadorPostagem; ?>').isOnScreen() == true) {
						$('#animar<?php echo $contadorPostagem; ?>').removeClass("animar");
						$('#animar<?php echo $contadorPostagem; ?>').addClass("animado");
					}
				});
			});
		</script>


		<section class="infoA" id="<?php echo $linkTituloProjeto ?>">


			<div class="row areaImagem">
				<div class="container">
					<div class="col-md-8">
						<img src="<?php echo $mockupPrincipal ?>" alt="" class="imgIlustrativa" class="img-responsive">
					</div>
					<div class="col-md-4">
						<div class="borda" title="<?php echo $tituloProjeto; ?>">
							<img src="<?php echo $iconeProjeto; ?>" alt="" class="logoservico" class="img-responsive">
						</div>
					</div>
				</div>
			</div>

			<div class="textoInfo">
				<div class="container">
				<div class="row" style="position: relative;height: 100px;">
					<div class="col-md-8"></div>
					<div class="col-md-4"><h2><?php echo $chamada1; ?> </h2></div>
				</div>

				<div class="textoDescrititvo">
					<p><?php echo $txtChamada1 ?></p>
				</div>
				</div>
			</div>

		</section>

		<section class="infoB">
			<div class="container">
				<h2><?php echo $chamada2; ?> </h2>
				<p><?php echo $txtChamada2 ?> </p>
			</div>

			<div class="row areaTextoImg">
				<div class="container">
					<div class="col-md-6">
						<div class="textoInfoB">
							<h2><?php echo $chamada3; ?> </h2>
							<p><?php echo $txtChamada3 ?></p>
						</div>
					</div>
					<div class="col-md-6 corFundo">

						<div id="carrossel-imagens-case" class="owl-carousel" style="margin-top: -50px;">

							<?php
							foreach ($arrayGaleriaCase as $item):
								$urlImagem = $item["full_url"];
								$urlVideo = $item["description"];
								$videoMatches = array();
								preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $urlVideo, $videoMatches);
								$videoYoutubeId = (count($videoMatches) > 0) ? $videoMatches[1] : '';
								$imgVideo = "https://img.youtube.com/vi/". $videoYoutubeId . "/hqdefault.jpg";
								if ($urlVideo == ""):
							?>
							<div class="item" style="position: relative; display: block; width: 100%; height: 265px; margin: 0 auto;">
								<a class="fancybox" rel="gallery1" href="<?php echo $urlImagem ?>">
									<img src="<?php echo $urlImagem ?>" alt="" class="">
								</a>
							</div>
							<?php else: ?>
							<div class="item" style="position: relative; display: block; width: 100%; height: 265px; margin: 0 auto;">
								<a class="fancybox fancybox.iframe"  rel="gallery1" href="<?php echo $urlVideo ?>">
									<img src="<?php echo $imgVideo ?>" alt="" class="">
									<i class="fa fa-play" style="font-size: 70px; color: #ef5078; position: absolute; top: 50%; left: 50%; transform: translate(-50%,-50%);"></i>
								</a>
							</div>
							<?php endif; endforeach; ?>
						</div>
						<button class="navegacaoCarrosselProjeTras hidden-xs" style=" border: none; background: transparent; font-size: 45px; color: #3b3666; position: absolute; top: 50%; left: 20px; transform: translateY(-50%); z-index: 9999; "><i class="fa fa-angle-left"></i></button>
						<button class="navegacaoCarrosselProjeFrente hidden-xs" style=" border: none; background: transparent; font-size: 45px; color: #3b3666; position: absolute; top: 50%; right: 20px; transform: translateY(-50%); z-index: 9999; "><i class="fa fa-angle-right"></i></button>
					</div>
				</div>
			</div>
		</section>

		<hr class="linhaDivisoria">

		<?php $layout++;else: ?>



		<section class="infoA" id="<?php echo $linkTituloProjeto ?>">


			<div class="row areaImagem">
				<div class="container">
					<div class="col-md-8">
						<img src="<?php echo $mockupPrincipal ?>" alt="" class="imgIlustrativa" class="img-responsive">
					</div>
					<div class="col-md-4">
						<div class="borda" title="<?php echo $tituloProjeto; ?>">
							<img src="<?php echo $iconeProjeto; ?>" alt="" class="logoservico" class="img-responsive">
						</div>
					</div>
				</div>
			</div>

			<div class="textoInfo">
				<div class="container">
				<div class="row" style="position: relative;height: 100px;">
					<div class="col-md-8"></div>
					<div class="col-md-4"><h2><?php echo $chamada1; ?> </h2></div>
				</div>

				<div class="textoDescrititvo">
					<p><?php echo $txtChamada1 ?></p>
				</div>
				</div>
			</div>

		</section>


		<section class="infoB">
			<div class="container">
				<h2><?php echo $chamada2; ?> </h2>
				<p><?php echo $txtChamada2 ?> </p>
			</div>

			<div class="row areaTextoImg">
				<div class="container">
					<div class="col-md-6">
						<div class="textoInfoB">
							<h2><?php echo $chamada3; ?> </h2>
							<p><?php echo $txtChamada3 ?></p>
						</div>
					</div>
					<div class="col-md-6 corFundo">
						<img src="<?php echo $mockupIpad; ?>" alt="" class="img-responsive">
					</div>
				</div>
			</div>
		</section>

		<hr class="linhaDivisoria">

		<?php  endif; endif; endif; endwhile; wp_reset_query(); endif;?>


		<?php
		$paginaAtual = get_the_title();
		$clientesPost = new WP_Query( array( 'post_type' => 'cliente') );
		$contPost = count($clientesPost->posts);

			$dados[0]    = get_next_post();
			$idTituloAnterior = $dados[0]->ID;
			$dados[1]    = get_previous_post();
			$idTituloProximo = $dados[1]->ID;
			if(get_the_title($idTituloProximo) != $paginaAtual && $dados[1] != ''):
				$fotos = rwmb_meta('Palupa_cliente_antiprox',$args ,$idTituloProximo );
				$cor = rwmb_meta('Palupa_cliente_cor',$args ,$idTituloProximo );
				$fotosProximoCliente = array();
				foreach ($fotos as $foto) {
					array_push($fotosProximoCliente, $foto['full_url']);
				}
		?>

		<a href="<?php echo get_permalink( $idTituloProximo ); ?>" title="<?php echo get_the_title($idTituloProximo); ?>">
			<section class="proximo-cliente">

				<div class="row">

					<div class=" col-xs-8 foto-esquerda" style=" background: url(<?php echo $fotosProximoCliente[0]; ?>) center center no-repeat;">

						<div class="nome-cliente">
							<h3><?php echo get_the_title($dados[1]->ID); ?></h3>
							<span style="background: <?php echo $cor; ?>;"></span>
							<p>ver case</p>
						</div>


					</div>

					<div class="col-xs-4 foto-direita" style=" background: url(<?php echo $fotosProximoCliente[1]; ?>) center center no-repeat;">


					</div>


				</div>

				<!-- LENTE ESCURA -->
				<div class="overlayPreto">

				</div>

			</section>

		</a>
		<?php else:
			$fotos = rwmb_meta('Palupa_cliente_antiprox',$args ,$idTituloAnterior );
			$cor = rwmb_meta('Palupa_cliente_cor',$args ,$idTituloAnterior );
			$fotosAnteriorCliente = array();
			foreach ($fotos as $foto) {
				array_push($fotosAnteriorCliente, $foto['full_url']);
			}
		?>
		<a href="<?php echo get_permalink( $idTituloAnterior ); ?>" title="<?php echo get_the_title($idTituloAnterior); ?>">
			<section class="proximo-cliente">

				<div class="row">

					<div class=" col-xs-8 foto-esquerda" style=" background: url(<?php echo $fotosAnteriorCliente[0]; ?>) center center no-repeat;">

						<div class="nome-cliente">
							<h3><?php echo get_the_title($dados[0]->ID); ?></h3>
							<span style="background: <?php echo $cor; ?>;"></span>
							<p>ver caser</p>
						</div>


					</div>

					<div class="col-xs-4 foto-direita" style=" background: url(<?php echo $fotosAnteriorCliente[1]; ?>) center center no-repeat;">


					</div>


				</div>

				<!-- LENTE ESCURA -->
				<div class="overlayPreto">

				</div>

			</section>

		</a>
		<?php endif; ?>

	</div>
</div>
<?php  get_footer(); ?>
<script>
	$(document).ready(function(){



		$('a[href*="#"]:not([href="#"])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				console.log(target.offset().top);
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top - 125
					}, 2000);
					return false;
				}
			}
		});
	});

</script>