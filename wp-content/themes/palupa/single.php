<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package palupa
 */
$categoriaAtual = get_the_category();
$categoriaAtual = $categoriaAtual[0]->cat_name;	
$autor = rwmb_meta('Palupa_video_autor');
get_header(); ?>
	
	<!-- PÁGINA DE POSTAGEM -->
	<div class="pg pg-postagem">
		
		<section class="sessaoPost">
			<?php 
				$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	            $fotoPost = $fotoPost[0];
			 ?>
			 <div class="sessaoPostFoto" style="background:url(<?php  echo $fotoPost ?>)">
			 	<div class="itemLente">
			 		<div class="carrosselDestaqueInformacoes">
			 			<span><?php echo $categoriaAtual ?></span>

			 			<h2><?php echo get_the_title() ?></h2>


			 		</div>
			 		<p>por <?php echo $autor ?></p>
			 	</div>
			 </div>

			<div class="container">
				<div class="sessaoPostTexto">
					<?php echo the_content() ?>
				</div>
			
			</div>
			<div class="disqus">
				<?php
					//If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>
			</div>
		</section>

		<!-- NOTÍCIAS -->
		<section class="sessaoNoticias">
			<p>Matérias relacionadas:</p>
			<ul>
				<?php 

					$i = 0;
					// LOOP DE POST NOTÍCIAS
					// LOOP DE DESTAQUES					
					$posts = new WP_Query(array(
						'post_type'     => 'post',
						'orderby' => 'rand',
						'posts_per_page'   => 3,

						'tax_query'     => array(
							array(
								'taxonomy' => 'category',
								'field'    => 'slug',
								'terms'    => $categoriaAtual,
								)
							)
						)
					);
					
					while ( $posts->have_posts() ) : $posts->the_post();
						$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoPost = $fotoPost[0];
				
					$categoriaAtualPost = get_the_category();
					foreach ($categoriaAtualPost as $categoriaAtualPost):
						$categoriaPost = $categoriaAtualPost;										
					endforeach;
								
											

				?>

				<li>
					<a href="<?php echo get_permalink(); ?>">
						<div class="sessaoNoticiasFoto" style="background:url(<?php echo $fotoPost ?>)"></div>

						<span style="color:<?php echo $categoriaPost->description ?>;"><?php echo $categoriaPost->name ?></span>

						<h2><?php echo get_the_title() ?></h2>

						
					</a>
				</li>
				<?php $i++; endwhile; wp_reset_query(); ?>

			</ul>
		</section>

	</div>

<?php

get_footer();
