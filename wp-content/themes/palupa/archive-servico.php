<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package palupa
 */
get_header(); ?>

	<!-- PÁGINA DE SERVIÇO -->
	<div class="pg pg-servico">

		<!-- TOPO FIXO ROXO -->
		<div class="topoFixoRoxo">

		</div>

		<!-- BTN MENU CLIENTE
		<div class="areaBtnMenuCliente">
			<button class="navTrigger navTriggerClientes" id="btnAbrirMenuLateral" ><i class="fa fa-th"></i></button>
			<button class="navTrigger navTriggerClientes" id="btnFecharMenuLateral"><i class="fa fa-times"></i></button>
			<p id="tituloMenuInternas">Serviços</p>
		</div>

		MENU CLIENTES
		<nav class="menu-lateral menu-lateralClientes">

			<ul class="listaMenuLateral">
				LOOP DE SERVIÇOS / SIDEBAR
				<?php
				while ( have_posts() ) : the_post();
				?>
				<li class="menu-item"><a href="<?php echo get_permalink(); ?>" title="<?php echo get_the_title() ?>"><?php echo get_the_title() ?></a></li>
				<?php endwhile; wp_reset_query(); ?>
			</ul>

		</nav> -->

		<!-- TÍTULO PÁGINA -->
		<div class="tituloPagina">
			<span>Serviços</span>
		</div>

		<?php
			// $i = 0;
			// if ( have_posts() ) :while ( have_posts() ) : the_post();
			// 	$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
			// 	$foto = $foto[0];
			// 	$subTitulo = rwmb_meta('Palupa_servico_subtitulo');
			// 	$itensListas = rwmb_meta('Palupa_servico_lista');
			// 	$iconServico = rwmb_meta('Palupa_servico_icone');
			// 	$iconServico = array_values($iconServico)[0];

		$categoriaServicoItems = get_terms( array( 'taxonomy' => 'categoriaServico', 'hide_empty' => false, ) );
		foreach ($categoriaServicoItems as $categoriaServicoItem):
		?>

			<!-- ANIMAÇÃO RODAPE -->
			<div class="areaAnimacaoRodape">
				<!-- SERVIÇO -->
				<div class="area-servico">

					<div class="container">
						<a href="<?php echo get_term_link($categoriaServicoItem, 'categoriaServico') ?>">
							<div class="titulo-servico">
								<div class="icone-servico">
									<img src="<?php echo get_term_meta( $categoriaServicoItem->term_id, 'icone', true ); ?>" alt="Ícone do serviço <?php echo $categoriaServicoItem->name ?>" class="img-responsive icon-svg">
									<img src="<?php bloginfo('template_directory'); ?>/img/logo-palupa.png" alt="" class="img-responsive logo">
								</div>
								<!-- TÍTULO-->
								<h4><?php echo $categoriaServicoItem->name ?></h4>
								<p class="vermais">Ver mais</p>
								<img class="setinha" src="<?php bloginfo('template_directory'); ?>/img/seta2.png" alt="">
							</div>
						</a>
					</div>


					<div class="dados-servico">
						<div class="container">
							<div class="row row-eq-height">
								<div class="col-md-6 borda-right">

									<!-- IMAGEM DO SERVICO -->
									<a href="<?php echo get_term_link($categoriaServicoItem, 'categoriaServico') ?>">
										<div class="imagem-servico">
											<img src="<?php echo get_term_meta( $categoriaServicoItem->term_id, 'imgLateral', true ); ?>" alt="Imagem lateral do serviço <?php echo $categoriaServicoItem->name ?>" class="img-responsive">
										</div>
									</a>
								</div>
								<div class="col-md-6">

									<!-- DADOS DO SERVIÇO -->
									<div class="dados">
										<!-- CHAMADA-->
										<strong class="chamada"><?php echo get_term_meta( $categoriaServicoItem->term_id, 'subTitulo', true ); ?></strong>

										<div class="conteudo-resumido">

											<!-- CONTEÚDO-->
											<div class="descricaoServico">
												<?php
												$qtdCaracteres = 140;
												$excerpt = $categoriaServicoItem->description;
												$qtdCaracteres++;
												if ( mb_strlen( $excerpt ) > $qtdCaracteres ) {
													$subex = mb_substr( $excerpt, 0, $qtdCaracteres - 5 );
													$exwords = explode( ' ', $subex );
													$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
													if ( $excut < 0 ) {
														echo trim(mb_substr( $subex, 0, $excut ));
													} else {
														echo trim($subex);
													}
													echo trim('...');
												} else {
													echo trim($excerpt);
												}
												?>
											</div>

											<a href="<?php echo get_term_link($categoriaServicoItem, 'categoriaServico') ?>" class="verMaisLink">
												<p class="vermais verMaisServicos">Ver mais </p>
												<img class="setinha" src="<?php bloginfo('template_directory'); ?>/img/seta.png" alt="">
											</a>

										</div>

										<!-- LOOP ITENS DA LISTA-->
										<?php
										$servicos = new WP_Query(array(
											'post_type' => 'servico',
											'posts_per_page' 	=> -1,
											'tax_query' 		=> array(
												array(
													'taxonomy' => 'categoriaServico',
													'field'    => 'slug',
													'terms'    => $categoriaServicoItem->slug,
													)
												)
											)
										);
										while ( $servicos->have_posts() ) : $servicos->the_post();
										?>

											<p class="servicoItem"><b><?php echo the_title(); ?>:</b> <?php trim(customExcerpt(100)); ?></p>

										<?php endwhile;wp_reset_query(); ?>

										<a class="btnVermais" href="<?php echo get_term_link($categoriaServicoItem, 'categoriaServico') ?>">Ver mais </a>
									</div>

								</div>
							</div>
						</div>
					</div>

				</div>
			</div>



		<?php
		endforeach;
		//$i++; endwhile;endif; wp_reset_query();
		?>


	</div>
<?php

get_footer();
