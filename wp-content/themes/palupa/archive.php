<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package palupa
 */

get_header(); ?>

	<<!-- PÁGINA DE BLOG	 -->
	<div class="pg pg-blog">
		
		<!-- CARROSSEL NOTÍCIAS -->
		<section class="carrosselNoticiasPG">
			
		
				<?php 
					// LOOP DE EXAMES
					if ( have_posts() ) : while( have_posts() ) : the_post();
			            $fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
			            $fotoPost = $fotoPost[0];
		           
				?>
				<div class="item" style="background:url(<?php echo $fotoPost  ?>)">					
					
					<div class="itemLente">
						<div class="carrosselDestaqueInformacoes">
							<!-- TÍTULO -->
							<h2><?php echo get_the_title() ?></h2>

							<!-- DESCRIÇÃO -->
							<p><?php customExcerpt(70); ?></p>
							
							<!-- LINK -->
							<a href="<?php echo get_permalink(); ?>" alt="">Ler mais</a>
						</div>	
					</div>
				</div>
				<?php    endwhile; endif;  ?>
				
			

		</section>	

		<!-- SESSÃO NOTÍCIAS -->
		<section class="sessaoNoticias">
			<div class="container">
				<div class="row">

					<div class="col-md-3">
						<div class="sideBar">
						
							<div class="areaPesquisa">
								<form>
									<input type="text" method="get" name="s" id="s"  id="searchform" action="<?php echo home_url('/'); ?>" class="pesquisar" placeholder="Pesquisar">
									<input type="submit" class="botaoPesquisar" value="&#xf002;">
								</form>
							</div>

							<?php

								// CATEGORIA ATUAL
								$categoriaAtual = get_the_category();
								$categoriaAtual = $categoriaAtual[0]->cat_name;

								
								// LISTA DE CATEGORIAS						
								$categorias=get_categories($args);
								foreach($categorias as $categoria) {
							?>
							<a href="<?php echo get_category_link($categoria->cat_ID); ?>" style="background:<?php echo $categoria->description ?>"><?php echo $categoria->name ?></a>
							<?php } ?>
						</div>
					</div>			

					<div class="col-md-9">
						<ul>

							<?php 
								if ( have_posts() ) : while( have_posts() ) : the_post();
						            $fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						            $fotoPost = $fotoPost[0];
					           
							?>

							<li>
								<a href="<?php echo get_permalink(); ?>">
									<div class="sessaoNoticiasFoto" style="background:url(<?php echo $fotoPost ?>)"></div>
									
									<?php 
										$categoriaAtualPost = get_the_category();
										foreach ($categoriaAtualPost as $categoriaAtualPost):
											$categoriaPost = $categoriaAtualPost;										
										endforeach;
									?>
									<span style="color:<?php echo $categoriaPost->description ?>;"><?php echo $categoriaPost->name ?></span>

									<h2><?php echo get_the_title() ?></h2>

									<p><?php customExcerpt(70); ?></p>

									<small>Ler mais</small>
								</a>
							</li>
						

							<?php    endwhile; endif;  ?>
							
							
						</ul>
					</div>
				</div>	

			</div>
		</section>

	</div>

<?php
get_footer();
