<?php
/**
 * palupa functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package palupa
 */

if ( ! function_exists( 'palupa_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function palupa_setup() {
    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on palupa, use a find and replace
     * to change 'palupa' to the name of your theme in all the template files.
     */
    load_theme_textdomain( 'palupa', get_template_directory() . '/languages' );

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support( 'post-thumbnails' );

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
        'primary' => esc_html__( 'Primary', 'palupa' ),
    ) );

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ) );

    /*
     * Enable support for Post Formats.
     * See https://developer.wordpress.org/themes/functionality/post-formats/
     */
    add_theme_support( 'post-formats', array(
        'aside',
        'image',
        'video',
        'quote',
        'link',
    ) );

    // Set up the WordPress core custom background feature.
    add_theme_support( 'custom-background', apply_filters( 'palupa_custom_background_args', array(
        'default-color' => 'ffffff',
        'default-image' => '',
    ) ) );
}
endif;
add_action( 'after_setup_theme', 'palupa_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function palupa_content_width() {
    $GLOBALS['content_width'] = apply_filters( 'palupa_content_width', 640 );
}
add_action( 'after_setup_theme', 'palupa_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function palupa_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'palupa' ),
        'id'            => 'sidebar-1',
        'description'   => '',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'palupa_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function palupa_scripts() {

    //FONTS
    wp_enqueue_style( 'google-font', 'https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,800italic,800,700italic,700,600italic,600,400italic|Montserrat:400,700');

    //JAVA SCRIPT
    wp_enqueue_script( 'palupa-jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js' );
    wp_enqueue_script( 'palupa-bootsrap', get_template_directory_uri() . '/js/bootstrap.min.js' );
    wp_enqueue_script( 'palupa-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js' );
    wp_enqueue_script( 'palupa-typed', get_template_directory_uri() . '/js/typed.js' );
    wp_enqueue_script( 'palupa-fancybox', get_template_directory_uri() . '/js/jquery.fancybox.pack.js' );
    wp_enqueue_script( 'palupa-geral', get_template_directory_uri() . '/js/geral.js' );

    //CSS
    wp_enqueue_style( 'palupa-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style( 'palupa-bootstrap-font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css');
    wp_enqueue_style( 'palupa-owlcarousel', get_template_directory_uri() . '/css/owl.carousel.css');
    wp_enqueue_style( 'palupa-animate', get_template_directory_uri() . '/css/animate.css');
    wp_enqueue_style( 'palupa-fancybox-style', get_template_directory_uri() . '/js/jquery.fancybox.css');
    wp_enqueue_style( 'palupa-style', get_stylesheet_uri() );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'palupa_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 *  Configurações via redux
 */
if (class_exists('ReduxFramework')) {
    require_once (get_template_directory() . '/redux/sample-config.php');
}

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

// VERSIONAMENTO DE FOLHAS DE ESTILO
function my_wp_default_styles($styles)
{
    $styles->default_version = "28112016";
}
add_action("wp_default_styles", "my_wp_default_styles");

// TAXONOMIA SERVICO CUSTOM FIELD
function categoriaServico_add_meta_fields( $taxonomy ) {
    ?>
    <div class="form-field term-group">
        <label for="subTitulo">Subtítulo</label>
        <input type="text" id="subTitulo" name="subTitulo" />
        <label for="icone">Url Ícone</label>
        <input type="text" id="icone" name="icone" />
        <label for="imgLateral">Url Imagem lateral</label>
        <input type="text" id="imgLateral" name="imgLateral" />
        <label for="bgTopo">Url Background topo</label>
        <input type="text" id="bgTopo" name="bgTopo" />
    </div>
    <?php
}
add_action( 'categoriaServico_add_form_fields', 'categoriaServico_add_meta_fields', 10, 2 );

function categoriaServico_edit_meta_fields( $term, $taxonomy ) {
    $subTitulo = get_term_meta( $term->term_id, 'subTitulo', true );
    $icone = get_term_meta( $term->term_id, 'icone', true );
    $imgLateral = get_term_meta( $term->term_id, 'imgLateral', true );
    $bgTopo = get_term_meta( $term->term_id, 'bgTopo', true );
    ?>
    <tr class="form-field term-group-wrap">
        <th scope="row">
            <label for="subTitulo">Subtítulo</label>
        </th>
        <td>
            <input type="text" id="subTitulo" name="subTitulo" value="<?php echo $subTitulo; ?>" />
        </td>
    </tr>
    <tr class="form-field term-group-wrap">
        <th scope="row">
            <label for="icone">Url Ícone</label>
        </th>
        <td>
            <input type="text" id="icone" name="icone" value="<?php echo $icone; ?>" />
        </td>
    </tr>
    <tr class="form-field term-group-wrap">
        <th scope="row">
            <label for="imgLateral">Url Imagem Lateral</label>
        </th>
        <td>
            <input type="text" id="imgLateral" name="imgLateral" value="<?php echo $imgLateral; ?>" />
        </td>
    </tr>
    <tr class="form-field term-group-wrap">
        <th scope="row">
            <label for="bgTopo">Url Background topo</label>
        </th>
        <td>
            <input type="text" id="bgTopo" name="bgTopo" value="<?php echo $bgTopo; ?>" />
        </td>
    </tr>
    <?php
}
add_action( 'categoriaServico_edit_form_fields', 'categoriaServico_edit_meta_fields', 10, 2 );

function categoriaServico_save_taxonomy_meta( $term_id, $tag_id ) {
    if( isset( $_POST['subTitulo'] ) ) {
        update_term_meta( $term_id, 'subTitulo', esc_attr( $_POST['subTitulo'] ) );
    }
    if( isset( $_POST['icone'] ) ) {
        update_term_meta( $term_id, 'icone', esc_attr( $_POST['icone'] ) );
    }
    if( isset( $_POST['imgLateral'] ) ) {
        update_term_meta( $term_id, 'imgLateral', esc_attr( $_POST['imgLateral'] ) );
    }
    if( isset( $_POST['bgTopo'] ) ) {
        update_term_meta( $term_id, 'bgTopo', esc_attr( $_POST['bgTopo'] ) );
    }
}
add_action( 'created_categoriaServico', 'categoriaServico_save_taxonomy_meta', 10, 2 );
add_action( 'edited_categoriaServico', 'categoriaServico_save_taxonomy_meta', 10, 2 );

function categoriaServico_add_field_columns( $columns ) {
    $columns['subTitulo'] = __( 'Subtítulo', 'subTitulo' );
    $columns['icone'] = __( 'Ícone', 'icone' );
    $columns['imgLateral'] = __( 'Imagem Lateral', 'imgLateral' );
    $columns['bgTopo'] = __( 'Background topo', 'bgTopo' );
    return $columns;
}
add_filter( 'manage_edit-categoriaServico_columns', 'categoriaServico_add_field_columns' );

function categoriaServico_add_field_column_contents( $content, $column_name, $term_id ) {
    switch( $column_name ) {
        case 'subTitulo' :
            $content = get_term_meta( $term_id, 'subTitulo', true );
            break;
        case 'icone' :
            $content = get_term_meta( $term_id, 'icone', true );
            break;
        case 'imgLateral' :
            $content = get_term_meta( $term_id, 'imgLateral', true );
            break;
         case 'bgTopo' :
            $content = get_term_meta( $term_id, 'bgTopo', true );
            break;
    }

    return $content;
}
add_filter( 'manage_categoriaServico_custom_column', 'categoriaServico_add_field_column_contents', 10, 3 );


// CUSTOM EXCERPT
function customExcerpt($qtdCaracteres) {
  $excerpt = get_the_excerpt();
  $qtdCaracteres++;
  if ( mb_strlen( $excerpt ) > $qtdCaracteres ) {
    $subex = mb_substr( $excerpt, 0, $qtdCaracteres - 5 );
    $exwords = explode( ' ', $subex );
    $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
    if ( $excut < 0 ) {
      echo trim(mb_substr( $subex, 0, $excut ));
    } else {
      echo trim($subex);
    }
    echo trim('...');
  } else {
    echo trim($excerpt);
  }
}


