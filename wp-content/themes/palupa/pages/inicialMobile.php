<?php
/**
 * Template Name: Inicial Mobile
 * Description: Página inicial do site da Palupa versão mobile
 *
 * @package palupa
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>

	<!-- FAVICON -->
	<link rel="shortcut icon" type="image/x-icon" href=" <?php echo get_template_directory_uri(); ?>/favicon.ico">

</head>

<body <?php body_class(); ?>>
	<div class="areaLoader">
		<div class="loader">
			<span class="icon-palupa"></span>
			<div class="slidingVertical">
				<?php
				$arrayFrases = array("Alimentando unicórnios...", "Servindo o café...", "Fazendo flexões...", "Observando o universo...", "Contando as galáxias...", "Consertando o Super Nintendo...", "Zerando Super Mario Bros...", "Cronometrando o tempo da cafeteira...", "Resolvendo equações de segundo grau...", "Pedindo a pizza...", "Estacionando cavalos-marinhos...", "Trabalhando na máquina do tempo...", "Criando nova versão do protótipo...", "Contando os mafagafinhos do ninho...", "Vestindo a armadura para a batalha...", "Sendo entrevistado pelo Jô...", "Plantando sementes coloridas..", "Batendo o recorde...", "Planejando a nova startup...", "Equipando os guerreiros...", "Fazendo brainstorm com Steve Jobs...", "Indo além...", "Aprendendo com os melhores" );
				$randomFrases = array_rand($arrayFrases, 4);
				foreach ($randomFrases as $randomFrase) {
					echo '<p>'.$arrayFrases[$randomFrase].'</p>';
				}
				?>
			</div>
		</div>
	</div>
	<header class="topoMobile">
		<a href="<?php echo home_url('/inicial-mobile'); ?>" title="Página inicial">
			<h1>Palupa Marketing & Dev</h1>
		</a>
	</header>

	<div class="pg pg-inicialMobile">

		<ul>

			<li>
				<a href="<?php echo home_url('/clientes'); ?>" class="itemMenuMobile" id="clientes">
					<div class="areaTxtItemMenu" style="background: url(<?php echo get_template_directory_uri(); ?>/img/clientesMobile.png) top center no-repeat;">

					</div>
				</a>
			</li>

			<li>
				<a href="<?php echo home_url('/servicos'); ?>" class="itemMenuMobile" id="servicos">
					<div class="areaTxtItemMenu" style="background: url(<?php echo get_template_directory_uri(); ?>/img/servicosMobile.png) right top no-repeat, url(<?php echo get_template_directory_uri(); ?>/img/servicosMobileTexto.png) center bottom no-repeat;">

					</div>
				</a>
			</li>

			<li>
				<a href="<?php echo home_url('/quem-somos'); ?>" class="itemMenuMobile" id="quemSomos">
					<div class="areaTxtItemMenu" style="background: url(<?php echo get_template_directory_uri(); ?>/img/quemSomosMobile.png) center center no-repeat;">

					</div>
				</a>
			</li>

			<li>
				<a href="<?php echo home_url('/contato'); ?>" class="itemMenuMobile" id="contato">
					<div class="areaTxtItemMenu" style="background: url(<?php echo get_template_directory_uri(); ?>/img/contatoMobile2.png) left center no-repeat;">

					</div>
				</a>
			</li>

		</ul>

	</div>
</body>
</html>