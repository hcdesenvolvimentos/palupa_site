<?php
/**
 * Template Name: Contato
 * Description: Página contato do site da Palupa
 *
 * @package palupa
 */
	global $configuracao;

	$endereco             = $configuracao['opt-endereco'];
	$enderecoGoogle       = 'https://www.google.com.br/maps/place/' . urldecode($endereco);
	$endereco2             = $configuracao['opt-endereco2'];
	$enderecoGoogle2       = 'https://www.google.com.br/maps/place/' . urldecode($endereco2);
	$telefone1            = $configuracao['opt-telefone1'];
	$telefone2            = $configuracao['opt-telefone2'];
	$email                = $configuracao['opt-email'];
	$telefoneFundo        = $configuracao['opt-contato-telefone-img']['url'];
	$emailFundo           = $configuracao['opt-contato-email-img']['url'];
	$ondeEstamosFundo     = $configuracao['opt-contato-onde-img']['url'];
	$emailEmprego         = $configuracao['opt-contato-email-interesse'];
	$horarioFuncionamento = $configuracao['opt-horario-funcionamento'];

get_header(); ?>

	<!-- MODAL CONTATO SUCESSO -->
	<div class="modal" id="modalContatoSucesso" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content modalContatoSucesso">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 345.834 345.834" xml:space="preserve">
					<g>
						<path d="M339.798,260.429c0.13-0.026,0.257-0.061,0.385-0.094c0.109-0.028,0.219-0.051,0.326-0.084   c0.125-0.038,0.247-0.085,0.369-0.129c0.108-0.039,0.217-0.074,0.324-0.119c0.115-0.048,0.226-0.104,0.338-0.157   c0.109-0.052,0.22-0.1,0.327-0.158c0.107-0.057,0.208-0.122,0.312-0.184c0.107-0.064,0.215-0.124,0.319-0.194   c0.111-0.074,0.214-0.156,0.321-0.236c0.09-0.067,0.182-0.13,0.27-0.202c0.162-0.133,0.316-0.275,0.466-0.421   c0.027-0.026,0.056-0.048,0.083-0.075c0.028-0.028,0.052-0.059,0.079-0.088c0.144-0.148,0.284-0.3,0.416-0.46   c0.077-0.094,0.144-0.192,0.216-0.289c0.074-0.1,0.152-0.197,0.221-0.301c0.074-0.111,0.139-0.226,0.207-0.34   c0.057-0.096,0.118-0.19,0.171-0.289c0.062-0.115,0.114-0.234,0.169-0.351c0.049-0.104,0.101-0.207,0.146-0.314   c0.048-0.115,0.086-0.232,0.128-0.349c0.041-0.114,0.085-0.227,0.12-0.343c0.036-0.118,0.062-0.238,0.092-0.358   c0.029-0.118,0.063-0.234,0.086-0.353c0.028-0.141,0.045-0.283,0.065-0.425c0.014-0.1,0.033-0.199,0.043-0.3   c0.025-0.249,0.038-0.498,0.038-0.748V92.76c0-4.143-3.357-7.5-7.5-7.5h-236.25c-0.066,0-0.13,0.008-0.196,0.01   c-0.143,0.004-0.285,0.01-0.427,0.022c-0.113,0.009-0.225,0.022-0.337,0.037c-0.128,0.016-0.255,0.035-0.382,0.058   c-0.119,0.021-0.237,0.046-0.354,0.073c-0.119,0.028-0.238,0.058-0.356,0.092c-0.117,0.033-0.232,0.069-0.346,0.107   c-0.117,0.04-0.234,0.082-0.349,0.128c-0.109,0.043-0.216,0.087-0.322,0.135c-0.118,0.053-0.235,0.11-0.351,0.169   c-0.099,0.051-0.196,0.103-0.292,0.158c-0.116,0.066-0.23,0.136-0.343,0.208c-0.093,0.06-0.184,0.122-0.274,0.185   c-0.106,0.075-0.211,0.153-0.314,0.235c-0.094,0.075-0.186,0.152-0.277,0.231c-0.09,0.079-0.179,0.158-0.266,0.242   c-0.099,0.095-0.194,0.194-0.288,0.294c-0.047,0.05-0.097,0.094-0.142,0.145c-0.027,0.03-0.048,0.063-0.074,0.093   c-0.094,0.109-0.182,0.223-0.27,0.338c-0.064,0.084-0.13,0.168-0.19,0.254c-0.078,0.112-0.15,0.227-0.222,0.343   c-0.059,0.095-0.12,0.189-0.174,0.286c-0.063,0.112-0.118,0.227-0.175,0.342c-0.052,0.105-0.106,0.21-0.153,0.317   c-0.049,0.113-0.092,0.23-0.135,0.345c-0.043,0.113-0.087,0.225-0.124,0.339c-0.037,0.115-0.067,0.232-0.099,0.349   c-0.032,0.12-0.066,0.239-0.093,0.36c-0.025,0.113-0.042,0.228-0.062,0.342c-0.022,0.13-0.044,0.26-0.06,0.39   c-0.013,0.108-0.019,0.218-0.027,0.328c-0.01,0.14-0.019,0.28-0.021,0.421c-0.001,0.041-0.006,0.081-0.006,0.122v46.252   c0,4.143,3.357,7.5,7.5,7.5s7.5-3.357,7.5-7.5v-29.595l66.681,59.037c-0.348,0.245-0.683,0.516-0.995,0.827l-65.687,65.687v-49.288   c0-4.143-3.357-7.5-7.5-7.5s-7.5,3.357-7.5,7.5v9.164h-38.75c-4.143,0-7.5,3.357-7.5,7.5s3.357,7.5,7.5,7.5h38.75v43.231   c0,4.143,3.357,7.5,7.5,7.5h236.25c0.247,0,0.494-0.013,0.74-0.037c0.115-0.011,0.226-0.033,0.339-0.049   C339.542,260.469,339.67,260.454,339.798,260.429z M330.834,234.967l-65.688-65.687c-0.042-0.042-0.087-0.077-0.13-0.117   l49.383-41.897c3.158-2.68,3.546-7.412,0.866-10.571c-2.678-3.157-7.41-3.547-10.571-0.866l-84.381,71.59l-98.444-87.158h208.965   V234.967z M185.878,179.888c0.535-0.535,0.969-1.131,1.308-1.765l28.051,24.835c1.418,1.255,3.194,1.885,4.972,1.885   c1.726,0,3.451-0.593,4.853-1.781l28.587-24.254c0.26,0.38,0.553,0.743,0.89,1.08l65.687,65.687H120.191L185.878,179.888z"></path>
						<path d="M7.5,170.676h126.667c4.143,0,7.5-3.357,7.5-7.5s-3.357-7.5-7.5-7.5H7.5c-4.143,0-7.5,3.357-7.5,7.5   S3.357,170.676,7.5,170.676z"></path>
						<path d="M20.625,129.345H77.5c4.143,0,7.5-3.357,7.5-7.5s-3.357-7.5-7.5-7.5H20.625c-4.143,0-7.5,3.357-7.5,7.5   S16.482,129.345,20.625,129.345z"></path>
						<path d="M62.5,226.51h-55c-4.143,0-7.5,3.357-7.5,7.5s3.357,7.5,7.5,7.5h55c4.143,0,7.5-3.357,7.5-7.5S66.643,226.51,62.5,226.51z"></path>
					</g>
				</svg>
				<strong>Sucesso!</strong>
				<p>Logo retornaremos o seu contato.</p>
				<button id="modalContatoSucessoFechar" class="btn btn-default" data-dismiss="modalContatoSucesso">Fechar</button>
			</div>
		</div>
	</div>

	<!-- MODAL CONTATO ERRO -->
	<div class="modal" id="modalContatoErro" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content modalContatoErro">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="554.625px" height="554.625px" viewBox="0 0 554.625 554.625" xml:space="preserve">
					<g>
						<g>
							<path d="M554.625,459l-76.5-130.05V133.875c0-21.038-17.213-38.25-38.25-38.25H38.25C17.212,95.625,0,112.837,0,133.875v229.5    c0,21.037,17.212,38.25,38.25,38.25h302.175L306,459H554.625z M453.263,120.487c3.825,3.825,5.737,7.65,5.737,13.388v162.562    l-28.688-47.812L388.237,321.3L306,248.625L453.263,120.487z M430.312,114.75l-191.25,166.388L47.812,114.75H430.312z     M24.862,376.763c-3.825-3.825-5.737-7.65-5.737-13.388v-229.5c0-5.737,1.913-9.562,5.737-13.388l147.263,128.138L24.862,376.763z     M47.812,382.5l139.612-120.487L239.062,306l51.638-43.987l87.975,76.5L351.9,382.5H47.812z M430.312,286.875l91.8,153H340.425    L430.312,286.875z"></path>
							<rect x="420.75" y="325.125" width="19.125" height="57.375"></rect>
							<rect x="420.75" y="401.625" width="19.125" height="19.125"></rect>
						</g>
					</g>
				</svg>
				<strong>Erro!</strong>
				<p>Houve um erro ao tentar enviar a sua mensagem. Verifique se todos os campos foram preenchidos corretamente.</p>
				<button id="modalContatoErroFechar" class="btn btn-default" data-dismiss="modalContatoErro">Fechar</button>
			</div>
		</div>
	</div>

	<!-- PÁGINA CONTATO -->
	<div class="pg pg-contato">

		<!-- TÍTULO PÁGINA -->
		<div class="tituloPagina">
			<span><?php echo the_title(); ?></span>
		</div>

		<!-- ANIMAÇÃO TOPO -->
		<div class="areaAnimacaoTopo">

			<!-- BANNER CONTATO -->
			<?php while (have_posts()): the_post(); ?>
				<?php $bannerContato = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'full') ); ?>
			<?php endwhile; ?>
			<div class="bannerContato" style="background: url(<?php echo $bannerContato; ?>) center center no-repeat;">

			</div>

			<!-- ÁREA INFO CONTATO -->
			<div class="areaInfoContato">

				<ul class="listaInfoContato">

					<li class="infoContato" style="background: url(<?php echo $telefoneFundo; ?>)center center no-repeat;">

						<!-- ÁREA TXT INFO CONTATO -->
						<div class="areaTxtInfoContato rosa">

							<div class="txtInfoContato">
								<strong class="txtRoxo">Telefone</strong>
								<span style=" height: 140px; "><?php if(isset($telefone1) && !empty($telefone1)): ?><a href="tel:+<?php echo preg_replace("/[^0-9]/", "", $telefone1); ?>" title="<?php echo $telefone1; ?>"><?php echo $telefone1; ?></a><br><?php endif; ?><?php if(isset($telefone2) && !empty($telefone2)): ?><a href="tel:+<?php echo preg_replace("/[^0-9]/", "", $telefone2); ?>" title="<?php echo $telefone2; ?>"><?php echo $telefone2; ?></a><?php endif; ?></span>
							</div>

						</div>

						<!-- BORDA RODAPÉ -->
						<div class="bordaRodape">

						</div>

						<!-- ÁREA ÍCONE CONTATO -->
						<div class="areaIconeContato">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="42.062px" height="42.062px" viewBox="0 0 42.062 42.062" xml:space="preserve">
								<g>
									<path d="M38.26,29.35l-1.327-1.327c4.337-4.337,4.336-11.394-0.002-15.731l1.329-1.328C43.329,16.032,43.329,24.281,38.26,29.35z    M35.964,13.259l-1.327,1.328c3.07,3.072,3.07,8.069,0,11.139l1.327,1.33C39.767,23.251,39.767,17.062,35.964,13.259z    M33.594,15.627l-1.327,1.328c1.768,1.765,1.768,4.639,0,6.403l1.328,1.328C36.093,22.189,36.093,18.125,33.594,15.627z    M3.802,29.35l1.327-1.327c-4.337-4.337-4.336-11.394,0.002-15.731l-1.329-1.328C-1.267,16.032-1.267,24.281,3.802,29.35z    M6.098,27.055l1.327-1.328c-3.07-3.07-3.07-8.068,0-11.14l-1.327-1.328C2.295,17.062,2.295,23.251,6.098,27.055z M8.467,24.687   l1.328-1.328c-1.766-1.765-1.766-4.638,0.001-6.403l-1.328-1.328C5.969,18.125,5.969,22.189,8.467,24.687z M29.745,8.057v25.949   c0,1.113-0.912,2.025-2.026,2.025H14.345c-1.114,0-2.026-0.912-2.026-2.025V8.057c0-1.114,0.912-2.026,2.026-2.026h13.374   C28.833,6.031,29.745,6.943,29.745,8.057z M18.646,7.737c0,0.135,0.11,0.245,0.246,0.245h4.278c0.136,0,0.245-0.11,0.245-0.245   c0-0.136-0.109-0.246-0.245-0.246h-4.278C18.757,7.491,18.646,7.601,18.646,7.737z M22.045,34.006c0-0.561-0.454-1.014-1.014-1.014   c-0.56,0-1.013,0.454-1.013,1.014c0,0.559,0.453,1.012,1.013,1.012C21.591,35.018,22.045,34.565,22.045,34.006z M28.335,9.244   H13.728v23.037h14.607V9.244z"></path>
								</g>
							</svg>
						</div>

					</li>

					<li class="infoContato" style="background: url(<?php echo $emailFundo; ?>)center center no-repeat;">

						<!-- ÁREA TXT INFO CONTATO -->
						<div class="areaTxtInfoContato roxo">

							<div class="txtInfoContato">
								<strong class="txtRosa">Email</strong>
								<span style=" height: 140px; "><a href="mailto:<?php echo $email; ?>" title="<?php echo $email; ?>"><?php echo $email; ?></a></span>
							</div>

						</div>

						<!-- BORDA RODAPÉ -->
						<div class="bordaRodape">

						</div>

						<!-- ÁREA ÍCONE CONTATO -->
						<div class="areaIconeContato">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="79.536px" height="79.536px" viewBox="0 0 79.536 79.536" xml:space="preserve">
								<g>
									<path d="M39.773,1.31L0,31.004v47.222h79.536V31.004L39.773,1.31z M28.77,22.499   c1.167-2.133,2.775-3.739,4.815-4.805c2.035-1.075,4.357-1.616,6.983-1.616c2.214,0,4.191,0.435,5.921,1.292   c1.729,0.87,3.045,2.094,3.967,3.687c0.9,1.595,1.367,3.334,1.367,5.217c0,2.247-0.694,4.279-2.082,6.097   c-1.74,2.292-3.961,3.436-6.68,3.436c-0.732,0-1.279-0.122-1.654-0.38c-0.365-0.262-0.621-0.632-0.743-1.129   c-1.022,1.012-2.231,1.52-3.589,1.52c-1.465,0-2.679-0.507-3.643-1.509c-0.966-1.012-1.447-2.361-1.447-4.031   c0-2.084,0.578-3.966,1.743-5.672c1.416-2.084,3.218-3.13,5.424-3.13c1.571,0,2.731,0.601,3.475,1.805l0.331-1.468h3.5   l-1.998,9.479c-0.125,0.606-0.187,0.986-0.187,1.163c0,0.228,0.052,0.38,0.149,0.497c0.099,0.111,0.223,0.165,0.357,0.165   c0.436,0,0.979-0.248,1.646-0.769c0.901-0.663,1.627-1.574,2.181-2.695c0.554-1.129,0.839-2.299,0.839-3.508   c0-2.165-0.782-3.977-2.352-5.445c-1.573-1.45-3.77-2.185-6.578-2.185c-2.393,0-4.417,0.487-6.077,1.468   c-1.66,0.966-2.913,2.343-3.765,4.114c-0.839,1.76-1.258,3.607-1.258,5.52c0,1.856,0.479,3.552,1.411,5.074   c0.945,1.533,2.26,2.641,3.956,3.345c1.696,0.697,3.643,1.046,5.828,1.046c2.097,0,3.909-0.293,5.432-0.881   c1.522-0.587,2.739-1.457,3.666-2.641h2.807c-0.88,1.792-2.227,3.192-4.049,4.215c-2.092,1.163-4.64,1.74-7.644,1.74   c-2.918,0-5.426-0.487-7.542-1.468c-2.121-0.986-3.689-2.434-4.73-4.35c-1.028-1.918-1.535-4.008-1.535-6.268   C27.017,26.952,27.595,24.64,28.77,22.499z M2.804,31.941l29.344,19.68L2.804,74.333V31.941z M5.033,75.844l34.74-26.885   l34.729,26.885H5.033z M76.729,74.333L47.391,51.621l29.339-19.68V74.333z M41.205,24.661c0.466,0.531,0.699,1.295,0.699,2.292   c0,0.891-0.174,1.856-0.513,2.879c-0.334,1.036-0.743,1.826-1.209,2.361c-0.318,0.375-0.658,0.652-0.992,0.826   c-0.439,0.249-0.906,0.37-1.41,0.37c-0.674,0.006-1.23-0.264-1.691-0.794c-0.45-0.531-0.673-1.346-0.673-2.465   c0-0.839,0.158-1.805,0.487-2.889c0.329-1.088,0.81-1.916,1.453-2.509c0.647-0.588,1.346-0.881,2.1-0.881   C40.162,23.856,40.749,24.125,41.205,24.661z"></path>
								</g>
							</svg>
						</div>

					</li>

					<li class="infoContato" style="background: url(<?php echo $ondeEstamosFundo; ?>)center center no-repeat;">

						<!-- ÁREA TXT INFO CONTATO -->
						<div class="areaTxtInfoContato rosa">

							<div class="txtInfoContato">
								<strong class="txtRoxo">Onde Estamos?</strong>
								<span><?php echo $endereco; ?></span>
								<span><?php echo $endereco2; ?></span>
							</div>

						</div>

						<!-- BORDA RODAPÉ -->
						<div class="bordaRodape">

						</div>

						<!-- ÁREA ÍCONE CONTATO -->
						<div class="areaIconeContato">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32" xml:space="preserve">
								<g>
									<path d="M1.38,15.408l13.288-0.527l8.085-9.339l7.728,11.579L32,16.903l-9.09-12.91l-9.283,1.719v-1.65l-0.765-0.158l-2.304,0.487    V6.28L7.463,6.854L0,13.633v1.785C0,15.419,0.763,15.399,1.38,15.408z"></path>
									<path d="M22.702,6.342l-7.804,9.016L2.155,15.865v10.82l11.708,1.406v0.004l7.319-0.854v-8.43l3.368-0.005v8.041l5.396-0.661    v-8.99L22.702,6.342z M7.126,23.309L3.733,23.09v-4.33l3.393,0.002V23.309z M12.862,23.655l-3.945-0.253v-4.66l3.945,0.001V23.655    z M19.521,23.453l-4.165,0.273v-4.94l4.165-0.003V23.453z M28.971,22.861l-3.145,0.207v-4.275l3.145-0.004V22.861z"></path>
								</g>
							</svg>
						</div>

					</li>

				</ul>

			</div>

		</div>

		<!-- ANIMAÇÃO RODAPÉ -->
		<div class="areaAnimacaoRodape">

			<!-- ÁREA TXT / FORMULÁRIO -->
			<div class="areaTxtForm">

				<div class="container containerForm">

					<div class="row">
						<div class="col-md-6" style="display: none;">
							<div class="areaTxtContato">
								<!-- FRASE CONTATO -->
								<div class="fraseContato">
									<i class="fa fa-envelope-o fa-pull-left fa-border" aria-hidden="true"></i>
									<b>Entre em contato com a Palupa:</b>
									<p>Se preferir, utilize o formulário ao lado para enviar sua mensagem.</p>
								</div>

								<!-- FRASE CONTATO -->
								<div class="fraseContato">
									<i class="fa fa-users fa-pull-left fa-border" aria-hidden="true"></i>
									<b>Venha fazer parte da Palupa:</b>
									<p>Envie seu currículo para <a href="mailto:<?php echo $emailEmprego; ?>" title="<?php echo $emailEmprego; ?>"><?php echo $emailEmprego; ?></a>. Surgindo oportunidades, nós lembraremos de você!</p>
								</div>

								<!-- FRASE CONTATO -->
								<div class="fraseContato">
									<i class="fa fa-clock-o fa-pull-left fa-border" aria-hidden="true"></i>
									<b>Horário de funcionamento:</b>
									<p>Trabalhamos de segunda à sexta, das <?php echo $horarioFuncionamento; ?>.</p>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<!-- ÁREA FORM CONTATO -->
							<div class="areaFormContato">
								<strong>Contato</strong>
								<div class="form">

									<?php echo do_shortcode('[contact-form-7 id="47" title="Formulário de contato" html_name="contato-pagina"]'); ?>

								</div>
							</div>

						</div>
					</div>

				</div>

			</div>

		</div>

	</div>
	<?php get_footer(); ?>
	<script>
		$(document).ready(function() {
			$('.wpcf7-submit').click(function(){
				function show_popup(){

					if($("div.wpcf7-validation-errors").length >= 1) {
						$("#modalContatoErro").show();
						clearInterval(robo);
					}
					if($("div.wpcf7-mail-sent-ok").length >= 1) {
						$("#modalContatoSucesso").show();
						clearInterval(robo);
					}
				};

				var robo = setInterval( show_popup, 500 );

			});

			$("#modalContatoErroFechar").click(function(){
				$("#modalContatoErro").hide();
			});

			$("#modalContatoSucessoFechar").click(function(){
				$("#modalContatoSucesso").hide();
			});

		});
	</script>