<?php
/**
 * Template Name: Quem Somos
 * Description: Página quem somos do site da Palupa
 *
 * @package palupa
 */

	global $configuracao;
	$fraseDestaque     = $configuracao['opt-quem-somos-chamadaSobre'];
	$subtituloDestaque = $configuracao['opt-quem-somos-subtituloSobre'];
	$txtSobre          = $configuracao['opt-quem-somos-textoSobre'];
	$txtEquipe         = $configuracao['opt-quem-somos-textoTeam'];
	$fraseRodape       = $configuracao['opt-quem-somos-textoFrase'];
	$numeroContador    = $configuracao['opt-quem-somos-numeroFrase'];
	$txtSobreaprteTopo    = $configuracao['opt-quem-somos-TextoSobre'];
	$txtOquefazemos    = $configuracao['opt-quem-somos-oqueFazemos'];
	$txtOquefazemosA    = $configuracao['opt-quem-somos-oqueFazemosTextoA'];
	$txtOquefazemosB    = $configuracao['opt-quem-somos-oqueFazemosTextoB'];

get_header(); ?>
<!-- PÁGINA QUEM SOMOS -->
<div class="pg pg-quemsomos">

	<!-- TÍTULO PÁGINA -->
	<div class="tituloPagina">
		<span>Quem Somos</span>
	</div>

	<!-- ANIMAÇÃO TOPO -->
	<div class="areaAnimacaoTopo">
		<!-- CARROSSEL VIDEOS -->
		<ul class="slides">
		<?php if ($configuracao['opt-quem-somos-fraseSlider'] != ''): ?>
			<!-- <div class="areaTxtSlide">
				<p><?php echo $configuracao['opt-quem-somos-fraseSlider'] ?></p>
				<p><?php echo $configuracao['opt-quem-somos-fraseSliderB'] ?><span>|</span></p>
			</div> -->
		<?php endif; ?>
			<?php
			$videoPost     = new WP_Query( array( 'post_type' => 'video', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );
			$contadorVideo = 0;
			while ( $videoPost->have_posts() ) : $videoPost->the_post();

			$imgVideo  = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
			$imgVideo  = $imgVideo[0];
			$video = rwmb_meta('Palupa_video_link');

			if ($contadorVideo == 0):
			?>

			<input type="radio" name="radio-btn" id="img-<?php echo $contadorVideo; ?>" checked />
			<li class="slide-container">

				<?php if(isset($video) && !empty($video)): ?>
				<div class="slide active" id="slide-<?php echo $contadorVideo; ?>">
					<div class="slideAreaVideo">

						<div class="slideVideo" style="transform: translate3d(0px, 0.333333px, 0px); margin-left: 0px;">
							<video preload="auto" autoplay="" loop="" muted="" src="<?php echo $video; ?>"></video>
						</div>

					</div>
				</div>
				<?php endif; ?>
				<?php if(isset($imgVideo) && !empty($imgVideo)): ?>
				<img src="<?php echo $imgVideo; ?>" alt="Imagem equipe Palupa.">
				<?php endif; ?>

			</li>

			<?php else: ?>

			<input type="radio" name="radio-btn" id="img-<?php echo $contadorVideo; ?>" />
			<li class="slide-container">
				<?php if(isset($video) && !empty($video)): ?>
				<div class="slide active" id="slide-<?php echo $contadorVideo; ?>">
					<div class="slideAreaVideo">

						<div class="slideVideo" style="transform: translate3d(0px, 0.333333px, 0px); margin-left: 0px;">
							<video preload="auto" autoplay="" loop="" muted="" src="<?php echo $video; ?>"></video>
						</div>

					</div>
				</div>
				<?php endif; ?>
				<?php if(isset($imgVideo) && !empty($imgVideo)): ?>
				<img src="<?php echo $imgVideo; ?>" alt="Imagem equipe Palupa.">
				<?php endif; ?>

			</li>

			<?php endif; $contadorVideo++; endwhile; wp_reset_query(); ?>


			<li class="nav-dots">
				<?php
				$videoPost     = new WP_Query( array( 'post_type' => 'video', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );
				$contadorDot = 0;
				while ( $videoPost->have_posts() ) : $videoPost->the_post();
					if($contadorDot == 0):
				?>
				<label for="img-<?php echo $contadorDot; ?>" class="nav-dot active" data-slide="slide-<?php echo $contadorDot; ?>" id="img-dot-<?php echo $contadorDot; ?>"></label>
				<?php else: ?>
				<label for="img-<?php echo $contadorDot; ?>" class="nav-dot" data-slide="slide-<?php echo $contadorDot; ?>" id="img-dot-<?php echo $contadorDot; ?>"></label>
				<?php endif; $contadorDot++; endwhile; wp_reset_query(); ?>
			</li>
		</ul>
	</div>

	<!-- ANIMAÇÃO RODAPE -->
	<div class="areaAnimacaoRodape">
	
		<div class="areaQuemSomos">
			<section class="row sobre">
				<div class="col-md-4 correcaoY" style="min-height: 515px;">
					<div class="texto">

						<div class="aspas"></div>

						<p class="titulo-texto">
							<?php echo $fraseDestaque; ?>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 310.284 310.284" style="enable-background:new 0 0 310.284 310.284;" xml:space="preserve" width="100px" height="100px">
								<path d="M155.142,0C69.597,0,0,69.597,0,155.142s69.597,155.142,155.142,155.142s155.142-69.597,155.142-155.142  S240.688,0,155.142,0z M79.171,231.401c-1.746,1.182-6.129,2.222-8.693-0.625l-4.731-5.95c-2.288-3.869,0.483-7.457,2.277-8.945  c8.529-7.075,14.731-12.548,18.601-16.419c7.589-7.981,13.199-15.97,16.903-23.935c0.847-1.821-1.315-2.977-2.438-3.345  c-27.967-9.166-41.955-25.325-41.955-48.474c0-13.639,4.53-24.722,13.585-33.242c9.059-8.525,20.407-12.785,34.041-12.785  c12.146,0,22.909,5.539,32.283,16.621c9.165,10.438,13.744,21.735,13.744,33.881C152.789,163.78,128.251,198.185,79.171,231.401z   M185.61,231.401c-1.746,1.182-6.129,2.222-8.693-0.625l-4.731-5.95c-2.288-3.869,0.483-7.457,2.277-8.945  c8.528-7.075,14.731-12.548,18.601-16.419c7.589-7.981,13.199-15.97,16.904-23.935c0.847-1.821-1.315-2.977-2.438-3.345  c-27.967-9.166-41.955-25.325-41.955-48.474c0-13.639,4.53-24.722,13.585-33.242c9.06-8.525,20.407-12.785,34.041-12.785  c12.146,0,22.909,5.539,32.283,16.621c9.164,10.438,13.744,21.735,13.744,33.881C259.228,163.78,234.69,198.185,185.61,231.401z" fill="#ef5078"/>
							</svg>
						</p>

						<p class="subtitulo-texto" style="font-size: 18px;"><?php echo $subtituloDestaque; ?></p>

						<p class="textoAbaixoSubititulo"><?php echo $txtSobreaprteTopo ?></p>

					</div>
				</div>

				<div class="col-md-8" style="">
					<?php while (have_posts()): the_post(); ?>
						<?php $imgQuemSomos = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'large') ); ?>
					<?php endwhile; ?>
					<div class="figura" style="background: url(<?php echo $imgQuemSomos; ?>) center center no-repeat;">
						<div class="lente">
							<!-- <div class="areaTxtSlide">
								<p><?php echo $configuracao['opt-quem-somos-fraseSlider'] ?></p>
								<p><?php echo $configuracao['opt-quem-somos-fraseSliderB'] ?><span>|</span></p>
							</div> -->
							<script>
								$(function(){
									/*var timer = setInterval(function() {
										if ($("body").hasClass("loaded")) {
											console.log("oi");
											$("#typed").typed({
												strings: ["<?php echo $configuracao['opt-quem-somos-fraseSliderB'] ?>"],
									            typeSpeed: 100,
									            startDelay: 1000,
									            showCursor: true,
									            cursorChar: "|",
									        });
										    clearInterval(timer);
									   	}
									}, 200);*/
									$(document).ready(function() {
									    setInterval(function() {
											if ($(".areaTxtSlide").children().length > 0) {
												$(".areaTxtSlide").fadeOut(function(){
													$(".areaTxtSlide").empty();
												});
											}else{
												var texto1 = "<p><?php echo $configuracao['opt-quem-somos-fraseSlider'] ?></p>";
												var texto2 = '<b id="typed" style=" font-size: 35px; "></b>';
												$(".areaTxtSlide").hide();
												$(".areaTxtSlide").append(texto1);
												$(".areaTxtSlide").append(texto2);
												$(".areaTxtSlide").fadeIn();
												$("#typed").typed({
													strings: ["<?php echo $configuracao['opt-quem-somos-fraseSliderB'] ?>"],
										            typeSpeed: 75,
										            startDelay: 1000,
										            showCursor: true,
										            cursorChar: "|",
										        });
											}
										},7000);
									});

								});
							</script>
							<div class="areaTxtSlide">
								<!-- <p><?php echo $configuracao['opt-quem-somos-fraseSlider'] ?></p>
								<b id="typed" style=" font-size: 35px; "></b> -->
							</div>

						</div>
						<div class="icon-palupa"></div>
					</div>
				</div>
				<hr>
				<div class="col-md-12 sobre">
					<div class="texto beforeTop" style="padding:0;margin: 0 auto;max-width: 1690px;">
						<p><?php echo $txtSobre ?></p>
					</div>
				</div>

			</section>
		</div>
	
	</div>
	
	<?php if($txtOquefazemos !=0 || $txtOquefazemos != ""): ?>
	<div class="areaQuemSomos">
		<section class="areaTexto">
				<span><?php echo $txtOquefazemos ?></span>
				<div class="row">
					<div class="col-md-6">
						<div class="texto">
							<p>
								<?php echo $txtOquefazemosA ?>
							</p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="texto">
							<p>
								<?php echo $txtOquefazemosB ?>
							</p>
						</div>
					</div>
				</div>
		</section> 
	</div>
<?php endif; ?>

	<?php if($configuracao['opt-quem-somos-valoresA']  !=0 || $configuracao['opt-quem-somos-valoresA']  != ""): ?>
		<div class="tituloValores">
			<p>Nossos Valores</p>
		</div>
		<div class="areaQuemSomos">
			<section class="areaValores">
				<div class="row">

					<?php 


					$textoA = explode("|", $configuracao['opt-quem-somos-valoresA']);
					$textoB = explode("|", $configuracao['opt-quem-somos-valoresB']);
					$textoC = explode("|", $configuracao['opt-quem-somos-valoresC']);
					$textoD = explode("|", $configuracao['opt-quem-somos-valoresD']);
					$textoE = explode("|", $configuracao['opt-quem-somos-valoresE']);
					$textoF = explode("|", $configuracao['opt-quem-somos-valoresF']);
					?>
					<?php if($configuracao['opt-quem-somos-valoresA'] != 0 || $configuracao['opt-quem-somos-valoresA'] != ""): ?>
						<div class="col-md-4">
							<div class="texto">
								<h4><?php echo $textoA[0] ?></h4>
								<p><?php echo $textoA[1] ?></p>
							</div>
						</div>	
					<?php endif ?>

					<?php if($configuracao['opt-quem-somos-valoresB'] != 0 || $configuracao['opt-quem-somos-valoresB'] != ""): ?>
						<div class="col-md-4">
							<div class="texto">
								<h4><?php echo $textoB[0] ?></h4>
								<p><?php echo $textoB[1] ?></p>
							</div>
						</div>	
					<?php endif ?>

					<?php if($configuracao['opt-quem-somos-valoresC'] != 0 || $configuracao['opt-quem-somos-valoresC'] != ""): ?>
						<div class="col-md-4">
							<div class="texto">
								<h4><?php echo $textoC[0] ?></h4>
								<p><?php echo $textoC[1] ?></p>
							</div>
						</div>	
					<?php endif ?>


					<?php if($configuracao['opt-quem-somos-valoresD'] != 0 || $configuracao['opt-quem-somos-valoresD'] != ""): ?>
						<div class="col-md-4">
							<div class="texto">
								<h4><?php echo $textoD[0] ?></h4>
								<p><?php echo $textoD[1] ?></p>
							</div>
						</div>	
					<?php endif ?>

					<?php if($configuracao['opt-quem-somos-valoresE'] != 0 || $configuracao['opt-quem-somos-valoresE'] != ""): ?>
						<div class="col-md-4">
							<div class="texto">
								<h4><?php echo $textoE[0] ?></h4>
								<p><?php echo $textoE[1] ?></p>
							</div>
						</div>	
					<?php endif ?>

					<?php if($configuracao['opt-quem-somos-valoresF'] != 0 || $configuracao['opt-quem-somos-valoresF'] != ""): ?>
						<div class="col-md-4">
							<div class="texto">
								<h4><?php echo $textoF[0] ?></h4>
								<p><?php echo $textoF[1] ?></p>
							</div>
						</div>	
					<?php endif ?>




				</div>
			</section>
		</div>
	<?php endif ?>

	<div class="areaQuemSomos">
		<!-- CARROSSEL DE PARCEIROS -->
		<section class="carrosselParceiros">
			<?php 
				$clientesPost = new WP_Query( array( 'post_type' => 'parceiros', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );

				if ($clientesPost->have_posts()):


			 ?>
			<p>Parceiros <span>que nos fazem ir além. </span></p>

			<div class="owl-Carousel carrosselParceirosItem" id="carrosselParceirosItem">
				<?php
				
				while ( $clientesPost->have_posts() ) : $clientesPost->the_post();
				$parceirologo = rwmb_meta('Palupa_parceiros_logo_branca');
				foreach ($parceirologo  as $parceirologo) {
					$parceirologo = $parceirologo ;
				}
				?>
				<div class="item">
					<img src="<?php echo $parceirologo['full_url'] ?>" alt="<?php echo get_the_title() ?>" title="<?php echo get_the_title() ?>" class="img-responsive">
				</div>

			<?php endwhile; wp_reset_query();  ?>

	
		</div>
		<?php endif; ?>


		<!-- CARROSEL DE CLIENTES -->
		<?php
			 $clientesparceiros = new WP_Query( array( 'post_type' => 'cliente', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );

			 if ($clientesparceiros->have_posts()):
		  ?>
		<p>Mais do que clientes <span>parceiros de negócio. </span></p>

		<div class="owl-Carousel carrosselParceirosItem parceiros" id="carrosselClientesItem">
			<?php
			$clientesparceiros = new WP_Query( array( 'post_type' => 'cliente', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );
			while ( $clientesparceiros->have_posts() ) : $clientesparceiros->the_post();
			$logo = rwmb_meta('Palupa_cliente_logo_branca');
			foreach ($logo  as $logo) {
				$logo = $logo ;
			}
			?>
			<div class="item">
				<img src="<?php echo $logo['full_url'] ?>" alt="<?php echo get_the_title() ?>" title="<?php echo get_the_title() ?>" class="img-responsive">
			</div>
			<?php endwhile; wp_reset_query(); ?>
		</div>
		<?php endif; ?>
		</section>
	</div>

	<section class="row palupa-team">

		<div class="row info">
			<h5 class="titulo-info">Equipe</h5>
			<p><?php echo $txtEquipe; ?></p>
		</div>

		<div class="row team">

			<ul>
				<?php
				$equipePost     = new WP_Query( array( 'post_type' => 'equipe', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );
				while ( $equipePost->have_posts() ) : $equipePost->the_post();

				$fotoEquipe  = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
				$fotoEquipe  = $fotoEquipe[0];
				?>
				<li style=" background: url(<?php echo $fotoEquipe; ?>) no-repeat; height: 500px; margin-right: 1px; margin-bottom: 1px;">
					<div class="lente"></div>
					<div class="lente-hover">
						<h5><?php echo the_title(); ?></h5>
						<span>também é:</span>
						<p><?php echo the_content(); ?></p>
					</div>
					<div class="nome">
						<h5><?php echo the_title(); ?></h5>
						<p><?php echo rwmb_meta('Palupa_equipe_especialidade'); ?></p>
					</div>
				</li>
				<?php endwhile; wp_reset_query(); ?>
			</ul>

		</div>

	</section>

	<section class="frase" id="frase">
		<div class="figura">
			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 306.976 306.976" style="enable-background:new 0 0 306.976 306.976;" xml:space="preserve" width="35px" height="35px">
				<g>
					<path d="M230.879,144.952c-3.426-4.771-4.874-10.757-3.84-16.786l2.061-12.018l-8.73-8.51c-0.133-0.129-0.253-0.267-0.382-0.399   c-66.809,55.733-74.408,145.071-73.68,188.929c0.027,1.598,1.254,2.919,2.846,3.067c1.591,0.148,3.039-0.925,3.361-2.491   C170.944,206.99,205.797,164.569,230.879,144.952z" fill="#ef5078"/>
					<path d="M99.861,85.018L79.7,89.531l-7.712,19.167c-0.14,0.348-0.298,0.685-0.454,1.023c31.466,44.515,47.854,135.4,54.405,181.181   c0.154,1.074,1.111,1.846,2.193,1.773c1.082-0.073,1.925-0.968,1.934-2.052c0.797-98.746-14.309-164.631-29.555-205.773   C100.294,84.904,100.081,84.969,99.861,85.018z" fill="#ef5078"/>
					<path d="M237.742,244.904l-2.857-3.93c-2.312-3.18-3.69-6.801-4.133-10.52c-21.992,5.865-55.074,20.873-65.644,58.833   c-0.571,2.05,0.423,4.213,2.35,5.115c1.928,0.902,4.226,0.28,5.434-1.472c11.534-16.727,32.267-38.816,63.187-43.301   c0.017-0.048,0.029-0.097,0.046-0.146L237.742,244.904z" fill="#ef5078"/>
					<path d="M80.479,206.57c-2.011,2.813-4.686,5.177-7.904,6.851l-4.309,2.241l-0.597,4.82c-0.18,1.452-0.504,2.857-0.943,4.21   c18.465,20.857,32.072,49.507,40.295,70.078c0.551,1.379,2.056,2.118,3.486,1.716c1.429-0.403,2.326-1.818,2.077-3.283   C105.303,250.21,92.568,223.285,80.479,206.57z" fill="#ef5078"/>
					<path d="M250.59,109.166l-3.839,22.381c-0.153,0.89,0.213,1.79,0.944,2.321c0.413,0.3,0.903,0.453,1.395,0.453   c0.378,0,0.757-0.09,1.104-0.273l20.1-10.567l20.099,10.567c0.347,0.183,0.726,0.273,1.104,0.273c0.492,0,0.982-0.153,1.395-0.453   c0.731-0.531,1.097-1.431,0.944-2.321l-3.839-22.381l16.261-15.851c0.647-0.631,0.88-1.574,0.601-2.433   c-0.279-0.859-1.022-1.485-1.916-1.615l-22.472-3.265l-10.05-20.363c-0.4-0.81-1.225-1.323-2.128-1.323   c-0.903,0-1.728,0.513-2.128,1.323l-10.05,20.363l-22.472,3.265c-0.894,0.13-1.637,0.756-1.916,1.615   c-0.279,0.859-0.046,1.802,0.6,2.433L250.59,109.166z" fill="#ef5078"/>
					<path d="M95.493,65.501c0.882-0.197,1.575-0.878,1.788-1.756c0.213-0.878-0.09-1.8-0.783-2.38L72.588,41.36l2.925-31.037   c0.085-0.899-0.349-1.769-1.117-2.243c-0.382-0.235-0.814-0.353-1.245-0.353c-0.438,0-0.875,0.121-1.26,0.362L45.476,24.647   L16.861,12.275c-0.303-0.131-0.623-0.195-0.941-0.195c-0.553,0-1.1,0.193-1.537,0.564c-0.689,0.585-0.985,1.51-0.765,2.386   l7.585,30.238L0.593,68.658c-0.597,0.678-0.758,1.636-0.414,2.471c0.344,0.836,1.132,1.404,2.033,1.465l31.102,2.131l15.877,26.829   c0.429,0.725,1.208,1.165,2.042,1.165c0.06,0,0.12-0.002,0.181-0.007c0.901-0.068,1.684-0.642,2.022-1.48l11.637-28.921   L95.493,65.501z" fill="#ef5078"/>
					<path d="M302.251,235.51c-0.13-0.894-0.757-1.636-1.616-1.915l-14.62-4.745l-4.379-14.734c-0.257-0.866-0.984-1.511-1.874-1.663   c-0.134-0.023-0.268-0.034-0.401-0.034c-0.752,0-1.469,0.358-1.92,0.979l-9.031,12.439l-15.366-0.388   c-0.02,0-0.04-0.001-0.06-0.001c-0.88,0-1.69,0.488-2.1,1.269c-0.42,0.8-0.35,1.769,0.181,2.499l9.039,12.433l-5.118,14.495   c-0.301,0.852-0.092,1.8,0.539,2.447c0.454,0.465,1.069,0.716,1.699,0.716c0.245,0,0.493-0.038,0.734-0.116l14.618-4.755   l12.204,9.346c0.422,0.323,0.931,0.489,1.443,0.489c0.358,0,0.717-0.081,1.051-0.245c0.81-0.4,1.323-1.225,1.322-2.129   l-0.005-15.371l12.66-8.718C301.993,237.295,302.381,236.404,302.251,235.51z" fill="#ef5078"/>
					<path d="M63.347,195.678c0.801-0.417,1.297-1.252,1.278-2.156c-0.019-0.903-0.549-1.717-1.368-2.1l-13.925-6.51l-2.531-15.161   c-0.149-0.891-0.79-1.62-1.655-1.881c-0.226-0.068-0.457-0.101-0.685-0.101c-0.647,0-1.279,0.265-1.734,0.753l-10.494,11.232   l-15.202-2.278c-0.118-0.018-0.235-0.026-0.352-0.026c-0.77,0-1.501,0.375-1.948,1.019c-0.516,0.742-0.565,1.712-0.128,2.503   l7.439,13.452l-6.864,13.754c-0.403,0.808-0.313,1.775,0.233,2.495c0.454,0.598,1.156,0.938,1.89,0.938   c0.15,0,0.301-0.014,0.451-0.043l15.092-2.918l10.96,10.778c0.451,0.444,1.052,0.681,1.665,0.681c0.262,0,0.525-0.043,0.78-0.132   c0.853-0.297,1.463-1.053,1.574-1.949l1.889-15.255L63.347,195.678z" fill="#ef5078"/>
					<path d="M164.224,67.025l-1.845-4.493l-4.748-1.027c-3.545-0.767-6.747-2.343-9.427-4.526c-7.651,20.35-13.008,51.827-7.374,99.172   c0.303,2.548,4.02,2.518,4.262-0.036c2.083-21.962,7.451-57.474,21.156-85.27C165.465,69.654,164.781,68.381,164.224,67.025z" fill="#ef5078"/>
					<path d="M170.993,12.588l1.552,15.293l-11.718,9.948c-0.689,0.585-0.986,1.509-0.766,2.386c0.219,0.876,0.917,1.552,1.8,1.743   l15.024,3.25l5.84,14.219c0.343,0.836,1.131,1.404,2.032,1.466c0.055,0.004,0.109,0.006,0.163,0.006   c0.84,0,1.624-0.446,2.051-1.179l7.734-13.284l15.328-1.16c0.901-0.068,1.685-0.642,2.022-1.48c0.337-0.838,0.17-1.795-0.432-2.468   l-10.244-11.46l3.633-14.936c0.214-0.878-0.09-1.801-0.783-2.381c-0.435-0.364-0.977-0.554-1.524-0.554   c-0.324,0-0.65,0.066-0.957,0.202l-14.065,6.201L174.6,10.329c-0.382-0.236-0.814-0.353-1.246-0.353   c-0.437,0-0.875,0.121-1.26,0.362C171.329,10.817,170.902,11.689,170.993,12.588z" fill="#ef5078"/>
				</g>
			</svg>
			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 297 297" style="enable-background:new 0 0 297 297;" xml:space="preserve" width="45px" height="45px">
				<path d="M262.655,11.202h-39.041V9.884c0-5.458-4.425-9.884-9.883-9.884H83.269c-5.458,0-9.884,4.426-9.884,9.884v1.318h-39.04  c-5.458,0-9.883,4.425-9.883,9.884v54.029c0,37.247,20.16,58.044,56.813,58.772c3.445,8.52,7.957,16.27,13.484,22.951  c7.759,9.378,17.148,16.395,27.55,20.764v50.708h-39.04c-5.458,0-9.884,4.425-9.884,9.884v48.923c0,5.458,4.426,9.884,9.884,9.884  h130.463c5.458,0,9.883-4.426,9.883-9.884v-48.923c0-5.459-4.425-9.884-9.883-9.884h-39.04v-50.708  c10.401-4.369,19.791-11.386,27.55-20.764c5.527-6.681,10.039-14.432,13.484-22.951c36.652-0.728,56.812-21.524,56.812-58.772  V21.086C272.538,15.627,268.113,11.202,262.655,11.202z M44.229,75.115V30.969h29.156v60.454c0,7.696,0.703,15.179,2.045,22.34  C53.932,111.517,44.229,99.341,44.229,75.115z M203.848,248.076v29.156H93.152v-29.156H203.848z M154.925,228.31h-12.85v-45.787  c2.123,0.199,4.262,0.323,6.425,0.323s4.302-0.124,6.425-0.323V228.31z M148.5,163.078c-32.07,0-55.348-30.135-55.348-71.655V19.768  h110.695v71.655C203.848,132.943,180.57,163.078,148.5,163.078z M252.771,75.115c0,24.226-9.703,36.401-31.201,38.648  c1.342-7.161,2.044-14.644,2.044-22.34V30.969h29.157V75.115z" fill="#919699"/>
			</svg>
		</div>

		<p class="texto"><?php echo $fraseRodape; ?></p>

		<!-- <span class="numero stats-number" data-stop="<?php echo $numeroContador; ?>" id="contadorRodapeQuemSomos"><?php echo $numeroContador; ?></span><span class="numeroPorcentagem">%</span> -->

	</section>

</div>
<?php get_footer(); ?>
<script>
	/*
	CONTADOR NUMERAL E BARRA RODAPE SERVICOS
	*/
	$(document).ready(function() {

		$.fn.isOnScreen = function(){
			var viewport = {};
			viewport.top = $(window).scrollTop();
			viewport.bottom = viewport.top + $(window).height();
			var bounds = {};
			bounds.top = this.offset().top;
			bounds.bottom = bounds.top + this.outerHeight();
			return ((bounds.top <= viewport.bottom) && (bounds.bottom >= viewport.top));
		};

		var numQuemSomos = false;
		$( window ).scroll(function() {
			if ($('#contadorRodapeQuemSomos').isOnScreen() == true && numQuemSomos == false) {


				$(".stats-number").each(function() {
					var t = $(this);
					$({
						Counter: 0
					}).animate({
						Counter: t.attr("data-stop")
					}, {
						duration: 5e3,
						easing: "swing",
						step: function(a) {
							t.text(Math.ceil(a))
						}
					})
				});

				numQuemSomos = true;
			};
		});
	});
</script>