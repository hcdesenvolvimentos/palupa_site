<?php

/**
 * Plugin Name: Base Palupa.
 * Description: Controle base do tema Palupa.
 * Version: 0.1
 * Author: Agência Palupa
 * Author URI: http://www.palupa.com.br
 * Licence: GPL2
 */


function basePalupa () {

		// TIPOS DE CONTEÚDO
		conteudosPalupa();

		// TAXONOMIA
		taxonomiaPalupa();

		// META BOXES
		metaboxesPalupa();

		// SHORTCODES
		// shortcodesPalupa();

	    // ATALHOS VISUAL COMPOSER
	    //visualcomposerPalupa();

	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosPalupa (){

		// TIPOS DE CONTEÚDO
		tipoMarketing();

		tipoDepoimentos();

		tipoDestaques();

		tipoDesenvolvimento();

		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'marketing':
					$titulo = 'Nome da Case de Marketing';
				break;

				case 'depoimentos':
					$titulo = 'Nome da pessoa';
				break;

				case '':
					$titulo = 'Nome do destaque';
				break;

				case 'desenvolvimento':
					$titulo = 'Nome do Case de Desenvolvimento';
				break;

				default:
				break;
			}

		    return $titulo;

		}

	}


		// CUSTOM POST TYPE Marketing
		function tipoMarketing() {

			$rotulosMarketing = array(
									'name'               => 'Marketing',
									'singular_name'      => 'Marketing',
									'menu_name'          => 'Case de Marketing',
									'name_admin_bar'     => 'Case de Marketing',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar nova Case de Marketing',
									'new_item'           => 'Nova Case de Marketing',
									'edit_item'          => 'Editar Case de Marketing',
									'view_item'          => 'Ver Case de Marketing',
									'all_items'          => 'Todos os Case de Marketing',
									'search_items'       => 'Buscar Case de Marketing',
									'parent_item_colon'  => 'Dos Case de Marketing',
									'not_found'          => 'Nenhum Case de Marketing cadastrado.',
									'not_found_in_trash' => 'Nenhum Case de Marketing na lixeira.'
								);

			$argsMarketing 	= array(
									'labels'             => $rotulosMarketing,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-book',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'marketing' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('marketing', $argsMarketing);

		}


		// CUSTOM POST TYPE Desenvolvimento
		function tipoDesenvolvimento() {

			$rotulosDesenvolvimento = array(
									'name'               => 'Desenvolvimento',
									'singular_name'      => 'Desenvolvimento',
									'menu_name'          => 'Case de Desenvolvimento',
									'name_admin_bar'     => 'Case de Desenvolvimento',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo Case de Desenvolvimento',
									'new_item'           => 'Novo Case de Desenvolvimento',
									'edit_item'          => 'Editar Case de Desenvolvimento',
									'view_item'          => 'Ver Case de Desenvolvimento',
									'all_items'          => 'Todos os Case de Desenvolvimento',
									'search_items'       => 'Buscar Case de Desenvolvimento',
									'parent_item_colon'  => 'Dos Case de Desenvolvimento',
									'not_found'          => 'Nenhum Case de Desenvolvimento cadastrado.',
									'not_found_in_trash' => 'Nenhum Case de Desenvolvimento na lixeira.'
								);

			$argsDesenvolvimento 	= array(
									'labels'             => $rotulosDesenvolvimento,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-universal-access',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'desenvolvimento' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('desenvolvimento', $argsDesenvolvimento);

		}


		// CUSTOM POST TYPE DEPOIMENTOS
		function tipoDepoimentos() {

			$rotulosDepoimentos = array(
									'name'               => 'Depoimentos',
									'singular_name'      => 'depoimento',
									'menu_name'          => 'Depoimentos',
									'name_admin_bar'     => 'depoimentos',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo depoimento',
									'new_item'           => 'Novo depoimento',
									'edit_item'          => 'Editar depoimento',
									'view_item'          => 'Ver depoimento',
									'all_items'          => 'Todos os depoimentos',
									'search_items'       => 'Buscar depoimentos',
									'parent_item_colon'  => 'Dos depoimentos',
									'not_found'          => 'Nenhum depoimento cadastrado.',
									'not_found_in_trash' => 'Nenhum depoimento na lixeira.'
								);

			$argsDepoimentos 	= array(
									'labels'             => $rotulosDepoimentos,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-id-alt',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'depoimentos' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('depoimentos', $argsDepoimentos);

		}


		// CUSTOM POST TYPE DESTAQUES
		function tipoDestaques() {

			$rotulosDestaques = array(
									'name'               => 'Destaques',
									'singular_name'      => 'destaque',
									'menu_name'          => 'Destaques',
									'name_admin_bar'     => '',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo destaque',
									'new_item'           => 'Novo destaque',
									'edit_item'          => 'Editar destaque',
									'view_item'          => 'Ver destaque',
									'all_items'          => 'Todos os ',
									'search_items'       => 'Buscar ',
									'parent_item_colon'  => 'Dos ',
									'not_found'          => 'Nenhum destaque cadastrado.',
									'not_found_in_trash' => 'Nenhum destaque na lixeira.'
								);

			$argsDestaques 	= array(
									'labels'             => $rotulosDestaques,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-id-alt',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => '' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('', $argsDestaques);

		}


	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaPalupa () {

		taxonomiaCategoriaDesenvolvimento();

		taxonomiaCategoriaMarketing();


	}

		function taxonomiaCategoriaDesenvolvimento() {

			$rotulosCategoriaDesenvolvimento = array(
												'name'              => 'Categorias de Desenvolvimento',
												'singular_name'     => 'Categoria de Desenvolvimento',
												'search_items'      => 'Buscar categorias de Desenvolvimento',
												'all_items'         => 'Todas categorias de Desenvolvimento',
												'parent_item'       => 'Categoria de Desenvolvimento pai',
												'parent_item_colon' => 'Categoria de Desenvolvimento pai:',
												'edit_item'         => 'Editar categoria de Desenvolvimento',
												'update_item'       => 'Atualizar categoria de Desenvolvimento',
												'add_new_item'      => 'Nova categoria de Desenvolvimento',
												'new_item_name'     => 'Nova categoria',
												'menu_name'         => 'Categorias de Desenvolvimento',
											);

			$argsCategoriaDesenvolvimento 		= array(
												'hierarchical'      => true,
												'labels'            => $rotulosCategoriaDesenvolvimento,
												'show_ui'           => true,
												'show_admin_column' => true,
												'query_var'         => true,
												'rewrite'           => array( 'slug' => 'categoria-desenvolvimento' ),
											);

			register_taxonomy( 'categoriaDesenvolvimento', array( 'desenvolvimento' ), $argsCategoriaDesenvolvimento );

		}

		function taxonomiaCategoriaMarketing() {

			$rotulosCategoriaMarketing = array(
												'name'              => 'Categorias de Marketing',
												'singular_name'     => 'Categoria de Marketing',
												'search_items'      => 'Buscar categorias de Case de Marketing',
												'all_items'         => 'Todas categorias de Case de Marketing',
												'parent_item'       => 'Categoria de Case de Marketing pai',
												'parent_item_colon' => 'Categoria de Case de Marketing pai:',
												'edit_item'         => 'Editar categoria de Case de Marketing',
												'update_item'       => 'Atualizar categoria de Case de Marketing',
												'add_new_item'      => 'Nova categoria de Case de Marketing',
												'new_item_name'     => 'Nova categoria',
												'menu_name'         => 'Categorias de Case de Marketing',
											);

			$argsCategoriaMarketing 		= array(
												'hierarchical'      => true,
												'labels'            => $rotulosCategoriaMarketing,
												'show_ui'           => true,
												'show_admin_column' => true,
												'query_var'         => true,
												'rewrite'           => array( 'slug' => 'categoria-marketing' ),
											);

			register_taxonomy( 'categoriaMarketing', array( 'marketing' ), $argsCategoriaMarketing );

		}

    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesPalupa(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'Palupa_';

			// PÁGINAS
			$metaboxes[] = array(

				'id'			=> 'paginasMetabox',
				'title'			=> 'Detalhes da página',
				'pages' 		=> array( 'page' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
                        'name'  => 'Galeria de Fotos:',
                        'id'    => "{$prefix}pagina_galeria",
                        'desc'  => '',
                        'type'  => 'image'
                    )
				),
				'validation' 	=> array()
			);

			$metaboxes[] = array(

				'id'			=> 'detalhesDesenvolvimentoMetabox',
				'title'			=> 'Detalhes do Desenvolvimento',
				'pages' 		=> array( 'desenvolvimento' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Sobre o Cliente: ',
						'id'    => "{$prefix}desenvolvimento_cliente",
						'desc'  => '',
						'type'  => 'textarea'						
					),

					array(
                        'name'  => 'Logo Cliente:',
                        'id'    => "{$prefix}desenvolvimento_cliente_logo",
                        'desc'  => '',
                        'type'  => 'image',                        
                    ),    

					array(
						'name'  => 'Desafio: ',
						'id'    => "{$prefix}desenvolvimento_desafio",
						'desc'  => '',
						'type'  => 'textarea'						
					),  

					array(
						'name'  => 'Solução: ',
						'id'    => "{$prefix}desenvolvimento_solução",
						'desc'  => '',
						'type'  => 'textarea'						
					),

					array(
						'name'  => 'Projeto: ',
						'id'    => "{$prefix}desenvolvimento_projeto",
						'desc'  => '',
						'type'  => 'textarea'						
					),

					array(
                        'name'  => 'Galeria de Screens Mobile:',
                        'id'    => "{$prefix}desenvolvimento_galeria_mobile",
                        'desc'  => '',
                        'type'  => 'image'                       
                    ),

                    array(
						'name'  => 'Descrição Mobile: ',
						'id'    => "{$prefix}desenvolvimento_descricao_mobile",
						'desc'  => '',
						'type'  => 'textarea'						
					),

					array(
                        'name'  => 'Galeria de Screens Desktop:',
                        'id'    => "{$prefix}desenvolvimento_galeria_desktop",
                        'desc'  => '',
                        'type'  => 'image'
                    ),

                    array(
						'name'  => 'Descrição Desktop: ',
						'id'    => "{$prefix}desenvolvimento_descricao_desktop",
						'desc'  => '',
						'type'  => 'textarea'						
					),

					array(
						'name'  => 'Tags do Projeto: ',
						'id'    => "{$prefix}desenvolvimento_tags",
						'desc'  => '',
						'type'  => 'textarea'						
					) 
				)
			);


			//DETALHES DO CURSO
			$metaboxes[] = array(

				'id'			=> 'detalhesMarketingMetabox',
				'title'			=> 'Detalhes do Case de Marketing',
				'pages' 		=> array( 'marketing' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Sobre o Cliente: ',
						'id'    => "{$prefix}marketing_cliente",
						'desc'  => '',
						'type'  => 'textarea'						
					),

					array(
                        'name'  => 'Logo Cliente:',
                        'id'    => "{$prefix}marketing_cliente_logo",
                        'desc'  => '',
                        'type'  => 'image',                        
                    ),     

					array(
						'name'  => 'Desafio: ',
						'id'    => "{$prefix}marketing_desafio",
						'desc'  => '',
						'type'  => 'textarea'						
					),  

					array(
						'name'  => 'Solução: ',
						'id'    => "{$prefix}marketing_solução",
						'desc'  => '',
						'type'  => 'textarea'						
					),

					array(
						'name'  => 'Projeto: ',
						'id'    => "{$prefix}marketing_projeto",
						'desc'  => '',
						'type'  => 'textarea'						
					),

					array(
						'name'  => 'Tags do Projeto: ',
						'id'    => "{$prefix}marketing_tags",
						'desc'  => '',
						'type'  => 'textarea'						
					)      
				)
			);

			return $metaboxes;
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesPalupa(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerPalupa(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'basePalupa');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	//add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	basePalupa();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );