  $(function(){

    /***************************************
	    *		   CARROSSEL RODAPÉ        	   *
		****************************************/
			$('#carrossel-rodape').owlCarousel({

                responsiveClass: true,
                responsive: {
                  0: {
                    items: 1,
                  },
                  500: {
                    items: 2,
                  },
                  900: {
                    items: 3,
                  },
                  1000: {
                    items: 5,
                  }
                },
              center:true,
              loop: true,
              mouseDrag: true,
              autoplay:true,
              autoplayTimeout:31,
              smartSpeed:7500,

			});

  });