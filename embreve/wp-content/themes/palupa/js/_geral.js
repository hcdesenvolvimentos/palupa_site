  $(function(){

    /***************************************
	    *		   CARROSSEL RODAPÉ        	   *
		****************************************/
			$('#carrossel-rodape').owlCarousel({
				loop: true,
                responsiveClass: true,
                responsive: {
                  0: {
                    items: 1,
                  },
                  500: {
                    items: 2,
                  },
                  900: {
                    items: 3,
                  },
                  1000: {
                    items: 4,
                  }
                },
                autoplay: true
			});

  });