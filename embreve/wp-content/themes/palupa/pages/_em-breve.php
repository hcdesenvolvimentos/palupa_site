<?php
/**
 * Template Name: Em Breve
 * Description: Página Em Breve do site da Palupa
 *
 * @package palupa
 */

get_header(); ?>

<div class="pg pg-em-breve">
	<div class="fundo">
		<video loop muted autoplay poster="" id="video">
			<source src="<?php bloginfo('template_directory'); ?>/img/palu.mp4" type="video/mp4">
		</video>

		<div class="lente"></div>

		<div class="conteudo">
			<div class="row">

				<div class="col-sm-12">

					<div class="logo"><h1>Palupa</h1></div>

					<div class="info">
						<div class="texto">
							<!-- Só mais um momento... -->
						</div>
						<div class="titulo">
							Nosso site está quase pronto!
						</div>
					</div>

				</div>

			</div>
			<div class="row">

				<div class="col-sm-6 esquerda">
					<div class="sociais">

						<p><strong>Também estamos aqui:</strong></p>

						<ul>
							<li><a href="https://www.facebook.com/palupamkt/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="https://www.instagram.com/palupa_mkt/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
							<li><a href="https://www.linkedin.com/m/company/9472373/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						</ul>

						<div class="fb-page" data-href="https://www.facebook.com/palupamkt" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/palupamkt"><a href="https://www.facebook.com/palupamkt">Palupa</a></blockquote></div></div>

					</div>
				</div>

				<div class="col-sm-6 direita">
					<div class="contato">					
						<div class="formulario">
							<?php echo do_shortcode('[contact-form-7 id="22" title="Em breve"]'); ?>
						</div>
					</div>
				</div>

			</div>
		</div>

		<footer>

			<!-- CARROSSEL PARCEIROS -->
			

			<div class="container">
				<p>Nossos clientes: </p>
				<div class="row carrossel">				
					<div class="col-md-12 area-carrossel-rodape">
						<div id="carrossel-rodape" class="owl-Carousel">			  	

							<a href="http://www.zapatamexicanbar.com.br/" title="" target="_blank" class="item">
								<div>
									<img src="<?php bloginfo('template_directory'); ?>/img/za.png" alt="" class="img-responsive center-block" alt="">
								</div>
							</a>

							<a href="http://www.apolar.com.br/" title="" target="_blank" class="item">
								<div>
									<img src="<?php bloginfo('template_directory'); ?>/img/apolar.png" alt="" class="img-responsive center-block" alt="">
								</div>
							</a>

							<a href="http://www.cardiomed.com.br/" title="" target="_blank" class="item">
								<div>
									<img src="<?php bloginfo('template_directory'); ?>/img/cardiomed.png" alt="" class="img-responsive center-block" alt="">
								</div>
							</a>

							<a href="http://www.bullprime.com/" title="" target="_blank" class="item">
								<div>
									<img src="<?php bloginfo('template_directory'); ?>/img/bullprime.png" alt="" class="img-responsive center-block" alt="">
								</div>
							</a>

							<a href="http://www.dviczsorvetes.com.br/" title="" target="_blank" class="item">
								<div>
									<img src="<?php bloginfo('template_directory'); ?>/img/dvicz65.png" alt="" class="img-responsive center-block" alt="">
								</div>
							</a>

							<a href="http://www.audiotext.com.br/" title="" target="_blank" class="item">
								<div>
									<img src="<?php bloginfo('template_directory'); ?>/img/audio.png" alt="" class="img-responsive center-block" alt="">
								</div>
							</a>

							<a href="http://ciapastel.com.br/" title="" target="_blank" class="item">
								<div>
									<img src="<?php bloginfo('template_directory'); ?>/img/cia.png" alt="" class="img-responsive center-block" alt="">
								</div>
							</a>

							<a href="http://www.rossiresidencial.com.br/" title="" target="_blank" class="item">
								<div>
									<img src="<?php bloginfo('template_directory'); ?>/img/ross.png" alt="" class="img-responsive center-block" alt="">
								</div>
							</a>

							<a href="http://www.arnsdeoliveira.adv.br/" title="" target="_blank" class="item">
								<div>
									<img src="<?php bloginfo('template_directory'); ?>/img/arns.png" alt="" class="img-responsive center-block" alt="">
								</div>
							</a>

						</div>
					</div>
				</div>
			</div>

			<ul>
				<li class="email">contato@palupa.com.br</li>
				<li class="endereco">Av. Visconde de Guarapuava, 3965 - Curitiba PR</li>
				<li class="tel">+55 41 3323-9435</li>				
			</ul>

		</footer>

	</div>
</div>