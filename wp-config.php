<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'projetos_palupa_site');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{[sZvmt?+oJN+/PU,zR*wBr^x~eFq<Qbn@3.p%fSKrJ5MZ[#`PH|r6ndDI]Cdg<m');
define('SECURE_AUTH_KEY',  '->~9o^J`BeR$B4{PH8Kjak9]{v(FBR|4pv`}%C>NvoMQ<09FH{7(na.oVDjZpd4Q');
define('LOGGED_IN_KEY',    '>6CoA|9z=]Ew?(=^~e`K_X1lD||nT d?Q8vB?:%jQOEh ,GE8iO}(?[kr{&IK>-<');
define('NONCE_KEY',        '`/n s`!n9+4E3H0m7Sjes|jt`bwv!QU IC9JU-ttuD&++SGE3$zK48-:v25|wF~S');
define('AUTH_SALT',        '|OQ157HWnx,E1m?gNQO#cp_ Q#3FVZjp9$#I#L=b&nY*u1tY,!&rG[y-nP,}rqpC');
define('SECURE_AUTH_SALT', 'h8SDXun#3M[W1-#.m/O2dt^xRXkWq$J| Q]~_-@qmFV^(sMMOVmMAXCy0z$7[Zw-');
define('LOGGED_IN_SALT',   '9[YAyIqY|KuE|d(%z&6i0V/qa&b+-kUj<0v O3)hpQks8-1`? iN)pgJ*H#OgZo_');
define('NONCE_SALT',       'K!-(QsHwq|A-n`wR7QR,ZFx[ioiqs?G<V];G@i)r*4.zx|G_*BG6ZjQDEX>71wnQ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
